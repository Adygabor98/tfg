﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading.Tasks;

namespace Comunications
{
    public static class Requests
    {
        /********** CONNECTION AND GET THE DATABASE *********/

        public static IMongoDatabase database = new MongoClient("mongodb://localhost:27017").GetDatabase("tfg_Database");

        /// <summary>
        /// This is the GET METHODS section
        /// </summary>


        /********* CARRIER METHODS *********/

        /**
         * Get all Careers
         */
        public async static Task<List<BsonDocument>> GetCarriers()
        {
            var carrierDB = database.GetCollection<BsonDocument>("carrier");

            var carriers = await carrierDB.Find(new BsonDocument()).ToListAsync();

            return carriers;
        }

        /********** STUDENTS METHODS *********/
        /**
         * Get all Students
         */
        public async static Task<List<BsonDocument>> GetStudents()
        {
            var studentsDB = database.GetCollection<BsonDocument>("students");

            var students = await studentsDB.Find(new BsonDocument()).ToListAsync();

            return students;
        }
        /**
         * Get student by Id
         */
        public static BsonDocument GetStudentById(ObjectId id)
        {
            var studentsDB = database.GetCollection<BsonDocument>("students");

            var filter = new BsonDocument { { "_id", id } };
            var student = studentsDB.Find(filter).FirstOrDefault();

            return student;
        }
        /**
         * Get student by Surname
         */
        public static BsonDocument GetStudentBySurname(String surname)
        {
            var studentsDB = database.GetCollection<BsonDocument>("students");

            var filter = new BsonDocument { { "surname", surname } };
            var student = studentsDB.Find(filter).FirstOrDefault();

            return student;
        }

        /********** COURSE METHODS *********/

        /**
         * Get all Courses
         */
        public async static Task<List<BsonDocument>> GetCourses()
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            List<BsonDocument> courses = await coursesDB.Find(new BsonDocument()).ToListAsync();

            return courses;
        }
        /**
         * Get course by Id
         */
        public static BsonDocument GetCourseById(ObjectId id)
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            var filter = new BsonDocument { { "_id", id } };
            var course = coursesDB.Find(filter).FirstOrDefault();

            return course;
        }
        /**
         * Get course by Number
         */
        public static BsonDocument GetCourseByNumber(string number)
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            var filter = new BsonDocument { { "number", number } };
            var course = coursesDB.Find(filter).FirstOrDefault();

            return course;
        }
        /**
         * Get course by Name
         */
        public static BsonDocument GetCourseByName(string name)
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            var filter = new BsonDocument { { "name", name } };
            var course = coursesDB.Find(filter).FirstOrDefault();

            return course;
        }
        /**
         * Get courses by Teacher Id
         */
        public async static Task<List<string>> GetCoursesByTeacher(ObjectId teacher)
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            var filter = new BsonDocument { { "teacher", teacher } };
            var courses = await coursesDB.Find(filter).ToListAsync();
            List<string> result = new List<string>();
            foreach (var course in courses)
            {
                result.Add(course["name"].AsString);
            }

            return result;
        }

        /********** RELATION STUDENTS METHODS *********/

        /**
         * Get courses by the stydents id
         */
        public async static Task<List<BsonDocument>> GetRelationStudents(ObjectId studentId)
        {
            var relationDB = database.GetCollection<BsonDocument>("studentsCourses");

            var filter = new BsonDocument { { "student", studentId } };
            List<BsonDocument> courses = await relationDB.Find(filter).ToListAsync();

            return courses;
        }
        /**
         * Get courses by the teacher id
         */
        public async static Task<List<BsonDocument>> GetRelationsTeacherCourses(ObjectId teacher)
        {
            var relationDB = database.GetCollection<BsonDocument>("studentsCourses");

            var filter = new BsonDocument { { "teacher", teacher } };
            List<BsonDocument> courses = await relationDB.Find(filter).ToListAsync();

            return courses;
        }
        /**
         * Get students by course Id
         */
        public async static Task<List<BsonDocument>> GetStudentsById(ObjectId course)
        {
            var relationDB = database.GetCollection<BsonDocument>("studentsCourses");

            var filter = new BsonDocument { { "course", course } };
            List<BsonDocument> courses = await relationDB.Find(filter).ToListAsync();

            return courses;
        }

        /********** TEACHERS METHODS *********/

        /**
         * Get all teachers
         */
        public async static Task<List<BsonDocument>> GetTeachers()
        {
            var teachersDB = database.GetCollection<BsonDocument>("teachers");

            List<BsonDocument> teachers = await teachersDB.Find(new BsonDocument()).ToListAsync();

            return teachers;
        }
        /**
         * Get teacher by Id
         */
        public static BsonDocument GetTeacherById(ObjectId id)
        {
            var teachersDB = database.GetCollection<BsonDocument>("teachers");

            var filter = new BsonDocument { { "_id", id } };
            BsonDocument teacher = teachersDB.Find(filter).FirstOrDefault();

            return teacher;
        }

        /********** AGENDA METHODS *********/

        /**
         * Get agenda by teacher id
         */
        public static BsonDocument GetAgendaFromTeacher(ObjectId teacher)
        {
            var agendaDB = database.GetCollection<BsonDocument>("agendas");

            var filter = new BsonDocument { { "ProfessorId", teacher } };
            BsonDocument agenda = agendaDB.Find(filter).FirstOrDefault();

            return agenda;
        }

        // USER METHODS
        /**
         * Check is Administrador Global of the platform exists
         */
        public static BsonDocument GetAdministrator(string username, string password)
        {
            var userDB = database.GetCollection<BsonDocument>("users");

            var filter = new BsonDocument { { "username", username }, { "password", password } };
            BsonDocument admin = null;
            admin = userDB.Find(filter).FirstOrDefault();

            return admin;
        }


        /// <summary>
        /// This is the UPDATE and INSERT METHODS section
        /// </summary>

        /********** CAREER METHODS *********/
        /**
         * Insert a new career into platform
         */
        public static void InsertCarrier(BsonDocument carrier)
        {
            var carrierDB = database.GetCollection<BsonDocument>("carrier");

            carrierDB.InsertOne(carrier);
        }

        /********** STUDENTS METHODS *********/
        /**
         * Insert a new student into platform
         */
        public static void InsertStudent(BsonDocument student)
        {
            var studentsDB = database.GetCollection<BsonDocument>("students");

            studentsDB.InsertOne(student);
        }
        /**
         * Update student information
         */
        public static void UpdateStudent(ObjectId studentId, BsonDocument student)
        {
            var studentsDB = database.GetCollection<BsonDocument>("students");

            studentsDB.ReplaceOne(
                Builders<BsonDocument>.Filter.Eq("_id", studentId),
                student
            );
        }

        /********** COURSES METHODS *********/
        /**
         * Insert a new course into platform
         */
        public static void InsertCourse(BsonDocument course)
        {
            var coursesDB = database.GetCollection<BsonDocument>("courses");

            coursesDB.InsertOne(course);
        }
        /**
         * Update the course information
         */
        public static void UpdateCourse(ObjectId idCourse, BsonDocument course)
        {
            var courseDB = database.GetCollection<BsonDocument>("courses");

            courseDB.ReplaceOne(
                Builders<BsonDocument>.Filter.Eq("_id", idCourse),
                course
            );
        }

        /********** RELATION STUDENTS METHODS *********/
        /**
         * Insert a new relation student into platform
         */
        public static void InsertRelation(BsonDocument relation)
        {
            var relationDB = database.GetCollection<BsonDocument>("studentsCourses");

            relationDB.InsertOne(relation);
        }

        /********** TEACHER METHODS *********/
        /**
         * Insert a new teacher into platform
         */
        public static void InsertTeacher(BsonDocument teacher)
        {
            var teachersDB = database.GetCollection<BsonDocument>("teachers");

            teachersDB.InsertOne(teacher);
        }
        /**
         * Update the teacher information
         */
        public static void updateTeacher(ObjectId idTeacher, BsonDocument teacher)
        {
            var teacherDB = database.GetCollection<BsonDocument>("teachers");

            teacherDB.ReplaceOne(
                Builders<BsonDocument>.Filter.Eq("_id", idTeacher),
                teacher
            );
        }

        /********** AGENDA METHODS *********/
        /**
         * Insert a new agenda into platform
         */
        public static void InsertAgenda(BsonDocument agenda)
        {
            var agendasDB = database.GetCollection<BsonDocument>("agendas");

            agendasDB.InsertOne(agenda);
        }
        /**
         * Update agenda information
         */
        public static void UpdateAgenda(FilterDefinition<BsonDocument> filterArray, UpdateDefinition<BsonDocument> arrayUpdate)
        {
            var agendasDB = database.GetCollection<BsonDocument>("agendas");

            agendasDB.UpdateOne(filterArray, arrayUpdate);
        }

        /// <summary>
        /// This is the CALCULUS METHODS section
        /// </summary>        
        
        /**
         * Get all the clases done in a Month of a teacher
         */
        public static List<BsonDocument> GetClassesMonth(int month, ObjectId teacher)
        {
            BsonDocument agenda = GetAgendaFromTeacher(teacher);

            List<BsonDocument> result = new List<BsonDocument>();
            foreach (BsonDocument myClass in agenda["dates"].AsBsonArray)
            {
                var dayCourse = myClass["date"].ToUniversalTime().Day;
                var monthCourse = myClass["date"].ToUniversalTime().Month;
                var lastDay = DateTime.DaysInMonth(myClass["date"].ToUniversalTime().Year, monthCourse);

                if ((dayCourse == lastDay && monthCourse + 1 == month) || (dayCourse != lastDay && monthCourse == month))
                {
                    dayCourse = dayCourse == lastDay && monthCourse + 1 == month ? 0 : dayCourse;
                    var index = result.FindIndex(x => x["day"] == dayCourse);
                    if (index != -1)
                    {
                        result[index]["hours"] = result[index]["hours"].AsDecimal + decimal.Parse(myClass["hours"].AsString,
                            NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);
                    }
                    else
                    {
                        result.Add(new BsonDocument { { "day", dayCourse }, { "hours", decimal.Parse(myClass["hours"].AsString,
                            NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands) } });
                    }
                }

            }

            return result;
        }
        /**
         * Get all the courses information of the teacher in a determinate month
         */
        public static List<BsonDocument> GetCoursesTeacher(ObjectId teacher, int month)
        {
            BsonDocument agenda = GetAgendaFromTeacher(teacher);
            List<BsonDocument> result = new List<BsonDocument>();

            foreach (BsonDocument course in agenda["dates"].AsBsonArray)
            {
                var dayCourse = course["date"].ToUniversalTime().Day;
                var monthCourse = course["date"].ToUniversalTime().Month;
                var lastDay = DateTime.DaysInMonth(course["date"].ToUniversalTime().Year, monthCourse);

                if ((dayCourse == lastDay && monthCourse + 1 == month) || (dayCourse != lastDay && monthCourse == month))
                {
                    BsonDocument c = GetCourseById(course["course"].AsObjectId);

                    var index = result.FindIndex(x => x["name"] == c["name"]);
                    if (index == -1) // No existeix
                    {
                        double price = course["price"].AsDouble;
                        decimal hours = decimal.Parse(course["hours"].AsString, NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);
                        double total = price * (double)hours;

                        result.Add(new BsonDocument {
                            { "name", c["name"] },
                            { "total", total },
                            { "criteria", new BsonArray { new BsonDocument { { "hours", hours }, { "price", price}, { "total", hours }, { "numStudents", course["numberStudents"] } } } },
                            { "hours", hours }
                        });
                    }
                    else // Existeix el curs
                    {
                        decimal a = result[index]["hours"].AsDecimal;
                        decimal b = decimal.Parse(course["hours"].AsString, NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);
                        result[index]["hours"] = a + b;

                        result[index]["total"] = result[index]["total"].AsDouble + course["price"].AsDouble *
                            (double)decimal.Parse(course["hours"].AsString, NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);

                        var iFind = existCriteria(result[index]["criteria"].AsBsonArray, b, course["price"].AsDouble);
                        if (iFind == -1)
                        {
                            result[index]["criteria"].AsBsonArray.Add(
                                new BsonDocument { { "hours", b }, { "price", course["price"].AsDouble }, { "total", b }, { "numStudents", course["numberStudents"] }
                            });
                        }
                        else
                        {
                            result[index]["criteria"][iFind]["total"] = (decimal)result[index]["criteria"][iFind]["total"] + b;
                        }
                    }
                }
            }

            return result;
        }
        /**
         * Check if a criteria exist in the BsonArray criteria given
         */
        private static int existCriteria(BsonArray criteria, decimal hours, double price)
        {
            int result = -1;
            int i = 0;
            foreach (BsonDocument criteri in criteria)
            {
                if (criteri["hours"] == hours && criteri["price"] == price)
                {
                    result = i;
                    break;
                }
                i++;
            }

            return result;
        }
        /**
         * Get the price of the teacher for a determinate number of students and the type of the education
         */
        public static Double GetPriceForTeacher(int numberStudents, bool type)
        {
            var informationDB = database.GetCollection<BsonDocument>("informations");
            string valueType = type ? "UNI" : "OTHER";
            var filter = new BsonDocument { { "who", "teacher" } } & Builders<BsonDocument>.Filter.Lte(x => x["minimum"], numberStudents) &
                     Builders<BsonDocument>.Filter.Gte(x => x["maximum"], numberStudents) &
                     new BsonDocument { { "type", valueType } };
            BsonDocument price = informationDB.Find(filter).FirstOrDefault();

            return price["price"].AsDouble;
        }
        /**
         * Get the students of a month and filter by the payment type
         */
        public async static Task<List<BsonDocument>> GetStudentsSummaryMonth(int monthNum, string monthName, bool typePayment)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var coursesDB = database.GetCollection<BsonDocument>("courses");
            var relation = database.GetCollection<BsonDocument>("studentsCourses");
            var studentsDB = database.GetCollection<BsonDocument>("students");
            var informationDB = database.GetCollection<BsonDocument>("informations");

            List<BsonDocument> courses = await coursesDB.Find(_ => true).ToListAsync();
            courses = courses.FindAll(e => e["months"][(monthNum - 1)][monthName].AsBoolean);
            List<BsonDocument> result = new List<BsonDocument>();
            foreach (BsonDocument course in courses)
            {
                List<BsonDocument> studentsCourse = await relation.Find(new BsonDocument { { "course", course["_id"] } }).ToListAsync();
                foreach (BsonDocument student in studentsCourse)
                {
                    BsonDocument studentInfo = studentsDB.Find(new BsonDocument { { "_id", student["student"] } }).FirstOrDefault();
                    if (studentInfo["cash"].AsBoolean == typePayment)
                    {
                        result.Add(new BsonDocument
                        {
                            { "total", (double)0 },
                            { "hours", (decimal)0 },
                            { "idCourse", course["_id"] },
                            { "nameCourse", course["name"] },
                            { "idStudent", studentInfo["_id"] },
                            { "nameStudent", studentInfo["name"] },
                            { "teacherCourse", course["teacher"] },
                            { "typeCourse", course["type"].AsBoolean },
                            { "surnameStudent", studentInfo["surname"] },
                            { "criteria", new BsonArray { new BsonDocument {
                                { "hours", (decimal)0 },
                                { "price", (double)0},
                                { "total", (double) 0 },
                                { "numStudents", 0 }
                            } } }
                        });
                    }
                }
            }

            foreach (BsonDocument student in result)
            {
                BsonDocument agenda = GetAgendaFromTeacher(student["teacherCourse"].AsObjectId);

                foreach (BsonDocument course in agenda["dates"].AsBsonArray)
                {
                    var dayCourse = course["date"].ToUniversalTime().Day;
                    var monthCourse = course["date"].ToUniversalTime().Month;
                    var lastDay = DateTime.DaysInMonth(course["date"].ToUniversalTime().Year, monthCourse);

                    if (((dayCourse == lastDay && monthCourse + 1 == monthNum) || (dayCourse != lastDay && monthCourse == monthNum)) &&
                        (course["course"] == student["idCourse"] && course["students"].AsBsonArray.Contains(student["idStudent"])))
                    {
                        var studentsClass = course["students"].AsBsonArray.Count > 5 ? 5 : course["students"].AsBsonArray.Count;
                        var type = student["typeCourse"].AsBoolean ? "UNI" : "OTHER";
                        var filter = new BsonDocument { { "who", "students" }, { "students", studentsClass }, { "type", type } };

                        var price = (informationDB.Find(filter).FirstOrDefault())["price"].AsDouble;

                        decimal a = student["hours"].AsDecimal;
                        decimal b = decimal.Parse(course["hours"].AsString, NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);
                        student["hours"] = a + b;

                        student["total"] = student["total"].AsDouble + price *
                            (double)decimal.Parse(course["hours"].AsString, NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);

                        var iFind = existCriteria(student["criteria"].AsBsonArray, b, price);
                        if (iFind == -1)
                        {
                            student["criteria"].AsBsonArray.Add(
                                new BsonDocument { { "hours", b }, { "price", price }, { "total", b }, { "numStudents", course["numberStudents"] }
                            });
                        }
                        else
                        {
                            student["criteria"][iFind]["total"] = (decimal)student["criteria"][iFind]["total"] + b;
                        }
                    }
                }
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            return result;
        }



        public static void InsertInformation (double price, string type, string who, int minimum = 0, int maximum = 0, int students = 0)
        {
            var informationDB = database.GetCollection<BsonDocument>("informations");
            var info = new BsonDocument();
            if (who == "teacher")
            {
                info = new BsonDocument() {
                    { "minimum", minimum},
                    { "maximum", maximum},
                    { "price", price },
                    { "type", type },
                    { "who", who }
                };
            } else
            {
                info = new BsonDocument() {
                    { "students", students},
                    { "price", price },
                    { "type", type },
                    { "who", who }
                };
            }

            informationDB.InsertOne(info);
        }
        public async static void AddInformation()
        {
            var informationDB = database.GetCollection<BsonDocument>("informations");

            List<BsonDocument> data = await informationDB.Find(new BsonDocument()).ToListAsync();
            if (data.Count == 0)
            {
                InsertInformation(15.0, "UNI", "teacher", 1, 2);
                InsertInformation(16.5, "UNI", "teacher", 3, 4);
                InsertInformation(18.0, "UNI", "teacher", 5, 6);
                InsertInformation(19.5, "UNI", "teacher", 7, 8);
                InsertInformation(21.0, "UNI", "teacher", 9, 10);
                InsertInformation(22.5, "UNI", "teacher", 11, 12);
                InsertInformation(24.0, "UNI", "teacher", 13, 14);
                InsertInformation(25.5, "UNI", "teacher", 15, 16);

                InsertInformation(13.0, "OTHER", "teacher", 1, 2);
                InsertInformation(14.5, "OTHER", "teacher", 3, 4);
                InsertInformation(16.0, "OTHER", "teacher", 5, 6);

                InsertInformation(19.5, "UNI", "students", 0, 0, 1);
                InsertInformation(13.5, "UNI", "students", 0, 0, 2);
                InsertInformation(11.5, "UNI", "students", 0, 0, 3);
                InsertInformation(10.0, "UNI", "students", 0, 0, 4);
                InsertInformation(9.0, "UNI", "students", 0, 0, 5);

                InsertInformation(16.0, "OTHER", "students", 0, 0, 1);
                InsertInformation(11.5, "OTHER", "students", 0, 0, 2);
                InsertInformation(10.5, "OTHER", "students", 0, 0, 3);
                InsertInformation(9.50, "OTHER", "students", 0, 0, 4);
                InsertInformation(8.50, "OTHER", "students", 0, 0, 5);
            }
        }

        /// <summary>
        /// This is the DELETE METHODS section
        /// </summary>

        /********** RELATION STUDENTS METHODS *********/
        /**
         * Delete the relation of the course and students
         */
        public static void DeleteRelationCourse(ObjectId idCourse, ObjectId idStudent)
        {
            var relationDB = database.GetCollection<BsonDocument>("studentsCourses");
            var filter = Builders<BsonDocument>.Filter.Eq("course", idCourse) &
                Builders<BsonDocument>.Filter.Eq("student", idStudent);

            relationDB.DeleteOne(filter);
        }
    }
}
