﻿using CentreEstudisMontilivi.Altes.Courses;
using CentreEstudisMontilivi.Altes.Degrees;
using CentreEstudisMontilivi.Altes.Students;
using CentreEstudisMontilivi.Altes.Teachers;
using CentreEstudisMontilivi.Extra_Options;
using CentreEstudisMontilivi.Visualitzacions.Agenda;
using CentreEstudisMontilivi.Visualitzacions.Courses;
using CentreEstudisMontilivi.Visualitzacions.Students;
using CentreEstudisMontilivi.Visualitzacions.Teachers;
using Comunications;
using MongoDB.Bson;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para Administrator_Menu.xaml
    /// </summary>
    public partial class Administrator_Menu : Window
    {
        public static bool logged = false;
        public static string directory = "";
        public Administrator_Menu()
        {
            InitializeComponent();
            Requests.AddInformation();
            // This will get the current WORKING directory (i.e. \bin\Debug)
            string workingDirectory = Environment.CurrentDirectory;

            // This will get the current PROJECT directory
            directory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            // readingCsv();
            if (logged)
            {
                OpenDefaultView("Escull una opció per començar");
                menuLogged.Visibility = Visibility.Visible;
                menuLogout.Visibility = Visibility.Hidden;
            }
            else
            {
                titlePage.Content = "I N S C R I U R E   C L A S S E S";
                ViewsContainer.Content = new refresh_Screen(this, false);
                menuLogged.Visibility = Visibility.Hidden;
                menuLogout.Visibility = Visibility.Visible;
            }
        }
        public void readingCsv()
        {
            using (var reader = new StreamReader(@"C:\\Users\\UX433FA\\Desktop\\Personal Ady\\FORMACIÓ\\WPF\\WpfApp1\\Teachers.csv"))
            {
                List<string> listA = new List<string>();
                List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    listA.Add(values[0]);
                    listB.Add(values[1]);
                }
                Console.WriteLine("HOLA");
            }
        }
        public void OpenDefaultView( string message)
        {
            titlePage.Content = "";
            ViewsContainer.Content = new WelcomeScreen(message);
        }
        public void showSuccessScreen(BsonDocument options, Register_Student student, Register_Teacher teacher, Register_Course course, Registre_Degree degree)
        {
            titlePage.Content = "";
            if (student != null)
                ViewsContainer.Content = new InfoScreen(this, student, options);
            else if (teacher != null)
                ViewsContainer.Content = new InfoScreen(this, teacher, options);
            else if (course != null)
                ViewsContainer.Content = new InfoScreen(this, course, options);
            else if ( degree != null)
                ViewsContainer.Content = new InfoScreen(this, degree, options);

            else
                OpenDefaultView("Escull una opció per començar");
        }
        public void showSuccessScreenVisualitzation(BsonDocument options, Teachers_Screen teachers, Students_Screen students)
        {
            titlePage.Content = "";
            if (teachers != null)
                ViewsContainer.Content = new InfoScreen(this, teachers, options);
            else if ( students != null)
                ViewsContainer.Content = new InfoScreen(this, students, options);
            else
                OpenDefaultView("Escull una opció per començar");
        }
        public void loggedPlatform ()
        {
            logged = true;
            OpenDefaultView("Escull una opció per començar");
            menuLogged.Visibility = Visibility.Visible;
            menuLogout.Visibility = Visibility.Hidden;
        }
        private void closeOtherTabs(string typeOpen)
        {
            GridLengthConverter gridLengthConverter = new GridLengthConverter();

            switch (typeOpen)
            {
                case "register":
                    visualitzationExpander.IsExpanded = false;
                    visualitzationTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");

                    otherExpander.IsExpanded = false;
                    otherTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");
                    break;
                case "other":
                    visualitzationExpander.IsExpanded = false;
                    visualitzationTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");

                    altesExpander.IsExpanded = false;
                    altesTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");
                    break;
                default:
                    altesExpander.IsExpanded = false;
                    altesTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");

                    otherExpander.IsExpanded = false;
                    otherTab.Height = (GridLength)gridLengthConverter.ConvertFrom("30*");
                    break;
            }
        }
        public void openRegisterTab(object sender, RoutedEventArgs e)
        {
            if (!altesExpander.IsExpanded)
            {
                closeOtherTabs("register");
                GridLengthConverter gridLengthConverter = new GridLengthConverter();
                altesTab.Height = (GridLength)gridLengthConverter.ConvertFrom("64*");
            }

        }
        public void openVisualitzationTab(object sender, RoutedEventArgs e)
        {
            if (!visualitzationExpander.IsExpanded)
            {
                closeOtherTabs("visualitzation");
                GridLengthConverter gridLengthConverter = new GridLengthConverter();
                visualitzationTab.Height = (GridLength)gridLengthConverter.ConvertFrom("64*");
            }
        }
        public void openOtherTab(object sender, RoutedEventArgs e)
        {
            if (!otherExpander.IsExpanded)
            {
                closeOtherTabs("other");
                GridLengthConverter gridLengthConverter = new GridLengthConverter();
                otherTab.Height = (GridLength)gridLengthConverter.ConvertFrom("64*");
            }
        }
        public void OpenFormRegisterStudent(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "A L T A   A L U M N E";
            ViewsContainer.Content = new Register_Student(this);
        }
        public void OpenFormRegisterTeacher(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "A L T A   P R O F E S S O R";
            ViewsContainer.Content = new Register_Teacher(this);
        }
        public void OpenFormRegisterCourse(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "A L T A   C U R S";
            ViewsContainer.Content = new Register_Course(this);
        }
        public void OpenFormRegisterDegree(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "A L T A   C A R R E R A";
            ViewsContainer.Content = new Registre_Degree(this);
        }
        public void OpenVisualitzationStudents(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "A L U M N E S";
            ViewsContainer.Content = new Students_Screen(this);
        }
        public void OpenVisualitzationTeachers(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "P R O F E S S O R S";
            ViewsContainer.Content = new Teachers_Screen(this);
        }
        public void OpenVisualitzationCourses(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "C U R S O S";
            ViewsContainer.Content = new Courses_Screen(this);
        }
        public void OpenVisualitzationAgendaTeacher(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "V I S U A L I T Z A R   A G E N D A";
            ViewsContainer.Content = new Agenda_Screen();
        }
        public void OpenResumSalaryTeachers ( object sender, RoutedEventArgs e)
        {
            titlePage.Content = "R E S U M   S A L A R I   P R O F E S S O R S";

            BsonDocument options = new BsonDocument
            {
                { "option", "summarySalaries" },
                { "urlTemplate", directory+"\\WpfApp1\\Templates\\salaryTeachers.html" },
                { "urlResult", directory+"\\WpfApp1\\Result_Templates\\summarySalaryTeachers.html" }
            };
            ViewsContainer.Content = new webBrowser(options);
        }
        public void OpenSalaryTeacher(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "R E S U M   S A L A R I   P R O F E S S O R ";
            BsonDocument options = new BsonDocument
            {
                { "option", "summarySalary" },
                { "urlTemplate", directory+"\\WpfApp1\\Templates\\salaryTeacher.html" },
                { "urlResult", directory+"\\WpfApp1\\Result_Templates\\summarySalaryTeacher.html" }
            };
            ViewsContainer.Content = new webBrowser( options );
        }
        public void OpenEfectiveQuoteStudent(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "Q U O T E S   E F E C T I U   A L U M N E S";
            BsonDocument options = new BsonDocument
            {
                { "option", "efectiveQuoteStudent" },
                { "urlTemplate", directory+"\\WpfApp1\\Templates\\efectiveQuote.html" },
                { "urlResult", directory+"\\WpfApp1\\Result_Templates\\efectiveQuoteResult.html" }
            };
            ViewsContainer.Content = new webBrowser(options);
        }
        public void OpenBankQuoteStudent(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "Q U O T E S   B A N C   A L U M N E S";
            BsonDocument options = new BsonDocument
            {
                { "option", "bankQuoteStudent" },
                { "urlTemplate", directory+"\\WpfApp1\\Templates\\bankQuote.html" },
                { "urlResult", directory+"\\WpfApp1\\Result_Templates\\bankQuoteResult.html" }
            };
            ViewsContainer.Content = new webBrowser(options);
        }
        private void openLoginView(object sender, RoutedEventArgs e)
        {
            titlePage.Content = "BENVINGUT, INTRODUEIX LES CREDENCIALS";
            ViewsContainer.Content = new Login(this);
        }
        private void Loggout(object sender, RoutedEventArgs e)
        {
            logged = false;
            titlePage.Content = "I N S C R I U R E   C L A S S E S";
            ViewsContainer.Content = new refresh_Screen(this, false);
            menuLogged.Visibility = Visibility.Hidden;
            menuLogout.Visibility = Visibility.Visible;
        }
        public void openInputHours(object sender = null, RoutedEventArgs e = null)
        {
            titlePage.Content = "I N S C R I U R E   C L A S S E S";
            ViewsContainer.Content = new refresh_Screen(this, false);
            menuLogged.Visibility = Visibility.Hidden;
            menuLogout.Visibility = Visibility.Visible;
        }
        public void OpenInputViewHours( int day, string month, BsonDocument teacher)
        {
            titlePage.Content = "Informació del dia " + day + " " + month;
            ViewsContainer.Content = new inputHours_Screen(day, this, teacher);
        }
        public void openRefreshCalendar(BsonDocument t)
        {
            titlePage.Content = "I N S C R I U R E   C L A S S E S";
            ViewsContainer.Content = new refresh_Screen(this, true, t);
        }
    }
}
