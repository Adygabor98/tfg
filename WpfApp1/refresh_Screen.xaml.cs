﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using PdfSharp.Drawing.BarCodes;
using System;
using CsQuery.ExtensionMethods;
using Comunications;
using System.Windows.Input;
using System.Net.Mail;
using System.Net;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para refresh_Screen.xaml
    /// </summary>
    public partial class refresh_Screen : UserControl
    {
        public static Administrator_Menu objAdmin;
        public static string[] monthName = { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre" };
        public static string[] daysWeek = { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge" };
        public static string[] daysWeekEn = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
        public static List<BsonDocument> classesMonth = new List<BsonDocument>();
        public static List<BsonDocument> teachers = new List<BsonDocument>();
        public static BsonDocument teacher = null;
        public refresh_Screen(Administrator_Menu obj, bool screen, BsonDocument t = null)
        {
            InitializeComponent();
            objAdmin = obj;
            initializeView();
            if ( screen )
            {
                teacher = t;
                initializeCalendar();
            }
        }
        async public void initializeView()
        {
            teachers = await Requests.GetTeachers();
            foreach ( BsonDocument teacher in teachers)
            {
                listTeachers.Items.Add( teacher["name"] + " " + teacher["surname"] );
            }
        }
        public void initializeCalendar(object sender = null, RoutedEventArgs e = null)
        {
            // Getting Teacher
            if (teacher == null ) teacher = teachers[listTeachers.SelectedIndex];
            classesMonth = Requests.GetClassesMonth(DateTime.Now.Month, teacher["_id"].AsObjectId);

            // Create the Grid
            Grid DynamicGrid = new Grid();
            DynamicGrid.Width = 950;
            DynamicGrid.Height = 350;
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Center;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Center;
            DynamicGrid.ShowGridLines = false;
            DynamicGrid.Background = new SolidColorBrush(Colors.Transparent);

            // Create Rows
            for (var i = 0; i < 7; i++)
            {
                RowDefinition gridRow = new RowDefinition();
                gridRow.Height = new GridLength(50);

                DynamicGrid.RowDefinitions.Add(gridRow);
            }
            // Create Columns
            for (var i = 0; i < 7; i++)
            {
                ColumnDefinition gridCol = new ColumnDefinition();
                DynamicGrid.ColumnDefinitions.Add(gridCol);

                if (i <= 6)
                {
                    var course = new Label();

                    // changing styles of the text box
                    course.Content = daysWeek[i];
                    course.FontSize = 12;
                    course.FontWeight = FontWeights.Bold;
                    course.HorizontalContentAlignment = HorizontalAlignment.Center;
                    course.VerticalContentAlignment = VerticalAlignment.Center;
                    course.Background = new SolidColorBrush(Colors.Beige);
                    // set position inside the grid
                    Grid.SetRow(course, 0);
                    Grid.SetColumn(course, i);

                    // Add child to the grid
                    DynamicGrid.Children.Add(course);
                }
            }

            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var initialDay = 1;
            var row = 1; var column = daysWeekEn.IndexOf(x => x == startDate.DayOfWeek.ToString());
            for (var i = 1; i <= endDate.Day; i++)
            {
                // Add Grid Day View
                var gridView = new Grid();
                ColumnDefinition col_First = new ColumnDefinition();
                col_First.Width = new GridLength(40); gridView.ColumnDefinitions.Add(col_First);
                gridView.Background = new SolidColorBrush(Colors.Transparent);
                ColumnDefinition col_Second = new ColumnDefinition(); gridView.ColumnDefinitions.Add(col_Second);

                // Number Day
                var dayView = new TextBox();
                dayView.BorderThickness = new Thickness(1);
                dayView.IsReadOnly = true;
                dayView.Text = initialDay.ToString(); dayView.FontSize = 15;
                dayView.Style = (Style)FindResource("TextBoxStyles");
                dayView.Width = 40; dayView.Height = 50; dayView.PreviewMouseDown += inputarHores;
                dayView.Foreground = new SolidColorBrush(Colors.Black);
                dayView.FontWeight = FontWeights.Bold;
                dayView.Background = new SolidColorBrush(Colors.Transparent);
                dayView.HorizontalContentAlignment = HorizontalAlignment.Center;
                dayView.VerticalContentAlignment = VerticalAlignment.Top;
                Grid.SetColumn(dayView, 0); gridView.Children.Add(dayView);

                // Hours View
                var hoursView = new TextBox();
                var index = classesMonth.IndexOf(x => (Int32.Parse(x["day"].ToString()) + 1) == initialDay);
                if ( index != -1)
                    hoursView.Text = classesMonth[index]["hours"].ToString() + " h";
                hoursView.IsReadOnly = true; hoursView.FontWeight = FontWeights.Bold;
                hoursView.Style = (Style)FindResource("TextBoxStyles");
                hoursView.Width = 100; dayView.PreviewMouseDown += inputarHores;
                hoursView.HorizontalContentAlignment = HorizontalAlignment.Center;
                hoursView.VerticalContentAlignment = VerticalAlignment.Center;
                hoursView.Background = new SolidColorBrush(Colors.Transparent);
                hoursView.Foreground = new SolidColorBrush(Colors.DarkRed);
                hoursView.BorderThickness = new Thickness(1);
                hoursView.FontSize = 20; Grid.SetColumn(hoursView, 1);
                gridView.Children.Add(hoursView);

                // Set Position inside the grid
                Grid.SetRow(gridView, row);
                Grid.SetColumn(gridView, column);

                // Add child to the grid
                DynamicGrid.Children.Add(gridView);

                // Refresh variables
                if (column == 6)
                {
                    row++; column = 0; 
                }
                else
                {
                    column++;
                }
                initialDay++;
            }
            // Add the created grid to the real view
            container.Content = DynamicGrid;

            introView.Visibility = Visibility.Hidden;
            inputHoursContainer.Visibility = Visibility.Visible;
        }
        public void inputarHores(object sender, MouseButtonEventArgs e)
        {
            int day = Int32.Parse((sender as TextBox).Text);
            string month = "";
            if ( (DateTime.Now.Month - 1)==4 || (DateTime.Now.Month - 1)==7 || (DateTime.Now.Month - 1)==9 )
                month = "d'" + monthName[DateTime.Now.Month - 1];
            else
                month = "de " +monthName[DateTime.Now.Month - 1];
            objAdmin.OpenInputViewHours(day, month, teacher);
        }
        public void sendEmail(object sender, RoutedEventArgs e)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("adriagabor@gmail.com", "shyne1698"),
                EnableSsl = true,
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress("adriagabor@gmail.com"),
                Subject = "subject",
                Body = "<h1>Hello</h1>",
                IsBodyHtml = true,
            };
            mailMessage.To.Add("shyne1698@gmail.com");

            smtpClient.Send(mailMessage);

            MessageBox.Show("mail Send");

        }
    }
}
