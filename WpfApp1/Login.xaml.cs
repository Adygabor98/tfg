﻿using Comunications;
using MongoDB.Bson;
using System.Windows;
using System.Windows.Controls;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public static Administrator_Menu objAdmin;
        public Login(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
            errorLabel.Content = "Usuari o Contrasenya incorrecte!";
        }

        private void loginAdministrator(object sender, RoutedEventArgs e)
        {
            BsonDocument user = Requests.GetAdministrator(username.Text, password.Password);
            if ( user == null)
            {
                errorLabel.Visibility = Visibility.Visible;
            }
            else
            {
                errorLabel.Visibility = Visibility.Hidden;
                objAdmin.loggedPlatform();
            }
        }
    }
}
