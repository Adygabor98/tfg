﻿using CentreEstudisMontilivi.Altes.Courses;
using CentreEstudisMontilivi.Altes.Degrees;
using CentreEstudisMontilivi.Altes.Students;
using CentreEstudisMontilivi.Altes.Teachers;
using CentreEstudisMontilivi.Visualitzacions.Students;
using CentreEstudisMontilivi.Visualitzacions.Teachers;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para InfoScreen.xaml
    /// </summary>
    public partial class InfoScreen : UserControl
    {
        public static Administrator_Menu objAdmin;
        public static Register_Student objRegStudent = null;
        public static Register_Teacher objRegTeacher = null;
        public static Register_Course objRegCourse = null;
        public static Registre_Degree objRegDegree = null;
        public static Teachers_Screen objVizTeachers = null;
        public static Students_Screen objVizStudents = null;
        public InfoScreen(Administrator_Menu obj, Register_Student objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objRegStudent = objReg;
            initializeView(options);
        }
        public InfoScreen(Administrator_Menu obj, Register_Teacher objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objRegTeacher = objReg;
            initializeView(options);
        }
        public InfoScreen(Administrator_Menu obj, Register_Course objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objRegCourse = objReg;
            initializeView(options);
        }
        public InfoScreen(Administrator_Menu obj, Registre_Degree objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objRegDegree = objReg;
            initializeView(options);
        }
        public InfoScreen(Administrator_Menu obj, Teachers_Screen objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objVizTeachers = objReg;
            initializeView(options);
        }
        public InfoScreen(Administrator_Menu obj, Students_Screen objReg, BsonDocument options)
        {
            InitializeComponent();
            objAdmin = obj;
            objVizStudents = objReg;
            initializeView(options);
        }
        public void initializeView(BsonDocument options)
        {
            message.Text = options["message"].AsString;
            matricula.Visibility = options["matricula"].AsBoolean ? Visibility.Visible : Visibility.Hidden;
            nextOption.Content = options["new"].AsString;
        }
        private void showWelcomeScreen(object sender, RoutedEventArgs e)
        {
            objAdmin.OpenDefaultView("Escull una opció per començar");
        }
        private void imprimirMatricula(object sender, RoutedEventArgs e)
        {
            objRegStudent.imprimirMatricula();
        }
        private void registerNewElement(object sender, RoutedEventArgs e)
        {
            if (objRegStudent != null)
                objAdmin.OpenFormRegisterStudent();
            else if (objRegTeacher != null)
                objAdmin.OpenFormRegisterTeacher();
            else if (objVizTeachers != null)
                objAdmin.OpenVisualitzationTeachers();
            else if (objVizStudents != null)
                objAdmin.OpenVisualitzationStudents();
        }
    }
}
