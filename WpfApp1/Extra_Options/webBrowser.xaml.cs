﻿using Comunications;
using HtmlAgilityPack;
using java.lang;
using java.time;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentreEstudisMontilivi.Extra_Options
{
    /// <summary>
    /// Lógica de interacción para webBrowser.xaml
    /// </summary>
    public partial class webBrowser : UserControl
    {
        public static BsonDocument optionsUser;
        public static string[] monthYear = { "Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre" };
        public static string[] monthYearEn = { "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december" };
        public List<BsonDocument> teachers;
        public static int month;
        public static BsonDocument teacher;
        public static List<BsonDocument> studentsInformation;

        public static int step = 1;
        public webBrowser(BsonDocument options)
        {
            InitializeComponent();
            optionsUser = options;
            teachers = new List<BsonDocument>();
            studentsInformation = new List<BsonDocument>();
            initializeFirstStep();
        }
        public void initializeFirstStep()
        {
            selector.ItemsSource = monthYear;
            selector.SelectedIndex = DateTime.Now.Month - 1;
        }
        async public void initializeSecondStep()
        {
            titleProcess.Content = "Selecciona el professor";
            initialPoint.Foreground = new SolidColorBrush(Colors.Gray);
            initialPoint.FontSize = 17;
            middlePoint.Foreground = new SolidColorBrush(Colors.DarkRed);
            middlePoint.FontSize = 20;

            List<BsonDocument> documents = await Requests.GetTeachers();
            List<string> teachersList = new List<string>();
            foreach (var teacher in documents)
            {
                teachers.Add(teacher);
                var name = teacher["name"].AsString + " " + teacher["surname"].AsString;
                teachersList.Add(name);
            }
            selector.ItemsSource = teachersList;
        }
        public void initializeFinalStep()
        {
            initialPoint.Foreground = new SolidColorBrush(Colors.Gray);
            initialPoint.FontSize = 17;
            middlePoint.Foreground = new SolidColorBrush(Colors.Gray);
            middlePoint.FontSize = 17;
            finalPoint.Foreground = new SolidColorBrush(Colors.DarkRed);
            finalPoint.FontSize = 20;

            titleProcess.Content = "Son correctes les dades?";
            gridSelector.Visibility = Visibility.Hidden;
            monthName.Content = monthYear[month - 1];
            if (optionsUser["option"] == "summarySalary")
                teacherName.Content = teacher["name"] + " " + teacher["surname"];
            else
                labelTeacher.Visibility = Visibility.Hidden;

            finalStep.Visibility = Visibility.Visible;
        }
        public void initialitzeView()
        {
            step = 1;
            switch (optionsUser["option"].AsString)
            {
                case "summarySalaries":
                    generateSalaryTeachers();
                    break;
                case "summarySalary":
                    generateSalaryTeacher();
                    break;
                case "efectiveQuoteStudent":
                    generateEfectiveQuote();
                    break;
                case "bankQuoteStudent":
                    generateBankQuote();
                    break;
                default:
                    break;
            }
        }
        async public void generateSalaryTeachers(object sender = null, RoutedEventArgs e = null)
        {
            List<BsonDocument> teachers = await Requests.GetTeachers();
            double allTeacherssalary = 0;
            var now = DateTime.Now;

            HtmlDocument doc = new HtmlDocument();
            doc.Load(optionsUser["urlTemplate"].AsString);

            doc.GetElementbyId("month").InnerHtml = "RESUM " + (monthYear[(month) - 1].ToString()).ToUpper() + " " + now.Year.ToString();
            var a = doc.GetElementbyId("table");


            foreach (BsonDocument teacher in teachers)
            {
                // Get teachers classes
                double totalPrice = 0;
                List<BsonDocument> courses = Requests.GetCoursesTeacher(teacher["_id"].AsObjectId, month);

                HtmlNode nameTeacher = HtmlNode.CreateNode("<tr > " +
                    "<th class='tg-0pky'></th><th class='tg-c3ow' style = 'color: darkred; font-size: 16px'> Professor: " + teacher["name"] + " " +
                    teacher["surname"] +
                    "</th><th class='tg-0lax'></th></tr>");
                a.AppendChild(nameTeacher);

                HtmlNode header = HtmlNode.CreateNode("<tr><th class='tg-c3ow' style='color: black; font-size: 16px'> Nom del curs </th> <th class='tg-c3ow' " +
                    "style='color: black; font-size: 16px'> Hores </th>" +
                    "<th class='tg-c3ow' style='color: black; font-size: 16px'> Criteri </th><th class='tg-c3ow' style='color: black; font-size: 16px'>" +
                    " Total </th> </tr>");
                a.AppendChild(header);
                a.AppendChild(HtmlNode.CreateNode("<tr><th colspan='4'><hr style='border: 1px solid darkred; margin - right: 50px' /></th></tr>"));

                foreach (BsonDocument c in courses)
                {
                    // Calculate Criteria
                    string criteri = "";
                    totalPrice += c["total"].AsDouble;
                    foreach (BsonDocument h in c["criteria"].AsBsonArray)
                    {
                        if (criteri == "")
                            criteri += h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                        else
                            criteri += " + " + h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                    }

                    HtmlNode course = HtmlNode.CreateNode("<tr > " +
                    "<th class='tg-0pky' style='font-size: 10px'>" + c["name"] + "</th><th class='tg-c3ow' style='font-size: 10px'>" +
                    c["hours"] + "</th><th class='tg-c3ow' style='font-size: 10px'>" + criteri + "</th><th class='tg-0lax' style='font-size: 10px'>" +
                    c["total"] + " €" + "</th></tr>");
                    a.AppendChild(course);
                }
                a.AppendChild(HtmlNode.CreateNode("<tr> " +
                    "<th class='tg-0pky' style='color: darkred; font-size: 14px'></th><th class='tg-c3ow' style='font-size: 10px'></th><th class='tg-c3ow'" +
                    "style='font-size: 10px'></th><th class='tg-0lax' style='color: darkred; font-size: 14px'></th></tr>"));
                a.AppendChild(HtmlNode.CreateNode("<tr> " +
                    "<th class='tg-0pky' style='color: darkred; font-size: 12px'> Suma </th><th class='tg-c3ow' style='font-size: 10px'></th><th class='tg-c3ow'" +
                    "style='font-size: 10px'></th><th class='tg-0lax' style='color: darkred; font-size: 12px'>" +
                    totalPrice + " €" + "</th></tr>"));
                a.AppendChild(HtmlNode.CreateNode("<tr><th colspan='4'><hr style='border: 1px solid darkred; margin - right: 50px' /></th></tr>"));
                allTeacherssalary = allTeacherssalary + totalPrice;
            }

            a.AppendChild(HtmlNode.CreateNode("<tr> " +
                    "<th class='tg-0pky' style='color: darkred; font-size: 14px'> Suma Total </th><th class='tg-c3ow' style='font-size: 10px'></th><th class='tg-c3ow'" +
                    "style='font-size: 10px'></th><th class='tg-0lax' style='color: darkred; font-size: 14px'>" +
                    allTeacherssalary + " €" + "</th></tr>"));

            doc.Save(optionsUser["urlResult"].AsString);
            webBrowser1.Visibility = Visibility.Visible;
            resultView.Visibility = Visibility.Visible;
            selectGrid.Visibility = Visibility.Hidden;
            string html = File.ReadAllText(optionsUser["urlResult"].AsString);
            webBrowser1.NavigateToString(html);
        }
        public void generateSalaryTeacher(object sender = null, RoutedEventArgs e = null)
        {
            var now = DateTime.Now;
            double totalPrice = 0;
            List<BsonDocument> courses = Requests.GetCoursesTeacher(teacher["_id"].AsObjectId, month);

            HtmlDocument doc = new HtmlDocument();
            doc.Load(optionsUser["urlTemplate"].AsString);

            doc.GetElementbyId("nameTeacher").InnerHtml = teacher["name"] + " " + teacher["surname"];
            doc.GetElementbyId("month").InnerHtml = monthYear[(month) - 1];
            doc.GetElementbyId("dateValue").InnerHtml = now.Day.ToString() + "/" + month.ToString() + "/" + now.Year.ToString();

            var a = doc.GetElementbyId("table");
            HtmlNode header = HtmlNode.CreateNode("<tr style='font-weight: bold';color: darkred><td style='width: 150px'>" + "Curs" + "</td><th style='width: 100px'>" + "Hores" + "</th><th>" + "Criteri" + "</th><th>" + "Total" + "</th></tr>");
            a.AppendChild(header);

            foreach (BsonDocument c in courses)
            {
                string criteri = "";
                totalPrice += c["total"].AsDouble;
                foreach (BsonDocument h in c["criteria"].AsBsonArray)
                {
                    if (criteri == "")
                        criteri += h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                    else
                        criteri += " + " + h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                }
                HtmlNode course = HtmlNode.CreateNode("<tr><td style='font-size: 10px'>" + c["name"] + "</td><td style='text-align: center;font-size: 10px'>" + c["hours"] + "</td><td style='text-align: center;font-size: 10px'>" + criteri +
                    "</td><td style='text-align: center;font-size: 10px'>" + c["total"] + "</td></tr>");
                a.AppendChild(course);
            }

            doc.GetElementbyId("totalPrice").InnerHtml = totalPrice.ToString() + "€";
            doc.Save(optionsUser["urlResult"].AsString);
            webBrowser1.Visibility = Visibility.Visible;
            resultView.Visibility = Visibility.Visible;
            selectGrid.Visibility = Visibility.Hidden;
            string html = File.ReadAllText(optionsUser["urlResult"].AsString);
            webBrowser1.NavigateToString(html);
        }
        async public void generateEfectiveQuote(object sender = null, RoutedEventArgs e = null)
        {
            var now = DateTime.Now;

            List<BsonDocument> students = await Requests.GetStudentsSummaryMonth(month, monthYearEn[(month) - 1].ToString(), true);
            studentsInformation = students;
            // Creating the html document
            HtmlDocument doc = new HtmlDocument();
            doc.Load(optionsUser["urlTemplate"].AsString);

            // Adding the title of the page
            doc.GetElementbyId("month").InnerHtml = "Hores Efectiu " + (monthYear[(month) - 1].ToString()).ToUpper() + " del " + now.Year.ToString();

            // Creating table and adding the header
            var table = doc.GetElementbyId("table");

            HtmlNode header = HtmlNode.CreateNode(
                "<tr>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Cognoms </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Nom </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Curs </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Hores </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Criteri </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Total </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Pagat </th>" +
                 "</tr>"
            );
            table.AppendChild(header);

            foreach (BsonDocument student in students)
            {
                // Calculate Criteria
                string criteri = "";
                foreach (BsonDocument h in student["criteria"].AsBsonArray)
                {
                    if (h["hours"] != 0 && h["price"] != 0)
                    {
                        if (criteri == "")
                            criteri += h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                        else
                            criteri += " + " + h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                    }
                }
                // Add alumn to the table
                HtmlNode studentRow = HtmlNode.CreateNode(
                    "<tr>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["surnameStudent"] + "</th>" +
                        "<th  class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["nameStudent"] + "</th>" +
                        "<th  class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["nameCourse"] + "</th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["hours"] + "h </th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + criteri + "</th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["total"] + "€ </th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'><input type='checkbox'></th> " +
                     "</tr>"
                );

                table.AppendChild(studentRow);
            }

            doc.Save(optionsUser["urlResult"].AsString);
            webBrowser1.Visibility = Visibility.Visible;
            resultView.Visibility = Visibility.Visible;
            selectGrid.Visibility = Visibility.Hidden;
            string html = File.ReadAllText(optionsUser["urlResult"].AsString);
            webBrowser1.NavigateToString(html);
        }
        async public void generateBankQuote(object sender = null, RoutedEventArgs e = null)
        {
            var now = DateTime.Now;

            List<BsonDocument> students = await Requests.GetStudentsSummaryMonth(month, monthYearEn[(month) - 1].ToString(), false);

            // Creating the html document
            HtmlDocument doc = new HtmlDocument();
            doc.Load(optionsUser["urlTemplate"].AsString);

            // Adding the title of the page
            doc.GetElementbyId("month").InnerHtml = "Hores Banc " + (monthYear[(month) - 1].ToString()).ToUpper() + " del " + now.Year.ToString();

            // Creating table and adding the header
            var table = doc.GetElementbyId("table");

            HtmlNode header = HtmlNode.CreateNode(
                "<tr>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Cognoms </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Nom </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Curs </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Hores </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Criteri </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Total </th>" +
                    "<th class='tg-c3ow' style='color: darkred; font-size: 17px'> Pagat </th>" +
                 "</tr>"
            );
            table.AppendChild(header);

            foreach (BsonDocument student in students)
            {
                // Calculate Criteria
                string criteri = "";
                foreach (BsonDocument h in student["criteria"].AsBsonArray)
                {
                    if (h["hours"] != 0 && h["price"] != 0)
                    {
                        if (criteri == "")
                            criteri += h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                        else
                            criteri += " + " + h["total"].ToString() + "h *" + h["price"].ToString() + "€/h (" + h["numStudents"].ToString() + ")";
                    }
                }
                // Add alumn to the table
                HtmlNode studentRow = HtmlNode.CreateNode(
                    "<tr>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["surnameStudent"] + "</th>" +
                        "<th  class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["nameStudent"] + "</th>" +
                        "<th  class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["nameCourse"] + "</th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["hours"] + "h </th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + criteri + "</th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'>" + student["total"] + "€ </th>" +
                        "<th class='tg-c3ow' style='color: black; font-size: 12px; font-weight: normal'><input type='checkbox'></th> " +
                     "</tr>"
                );

                table.AppendChild(studentRow);
            }

            doc.Save(optionsUser["urlResult"].AsString);
            webBrowser1.Visibility = Visibility.Visible;
            resultView.Visibility = Visibility.Visible;
            selectGrid.Visibility = Visibility.Hidden;
            string html = File.ReadAllText(optionsUser["urlResult"].AsString);
            webBrowser1.NavigateToString(html);
        }
        private void nextActionEvent(object sender, RoutedEventArgs e)
        {
            if (step == 1)
            {
                month = selector.SelectedIndex + 1;
                step++;
                if (optionsUser["option"].AsString == "summarySalary") initializeSecondStep();
                else { step++; initializeFinalStep(); };
            } else if (step == 2)
            {
                teacher = teachers[selector.SelectedIndex];
                step++;
                initializeFinalStep();
            } else
            {
                initialitzeView();
                finalStep.Visibility = Visibility.Hidden;
            }
        }
        private void SendNotifications(object sender, RoutedEventArgs e)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("adriagabor@gmail.com", "shyne1698"),
                EnableSsl = true,
            };

            studentsInformation.ForEach(student =>
            {
                BsonDocument s = Requests.GetStudentById(student["idStudent"].AsObjectId);
                if (s["email"] != "") { 
                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress("adriagabor@gmail.com"),
                        Subject = "Quota mes " + monthYear[month - 1],
                        Body = "<h1>Hola " + s["name"] + " " + s["surname"] + "</h1>" +
                        "<br />" +
                        "<p> La quota del mes " + monthYear[month - 1] + " és de " + student["total"] + "€</p>",
                        IsBodyHtml = true,
                    };
                    mailMessage.To.Add(s["email"].AsString);

                    smtpClient.Send(mailMessage);
                }
            });

            
        }

    }
}
