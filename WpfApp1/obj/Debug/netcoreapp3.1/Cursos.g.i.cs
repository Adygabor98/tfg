﻿#pragma checksum "..\..\..\Cursos.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8FE378EBC8E9E38746F2DECFCA9AAA0672F3CD26"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace WpfApp1 {
    
    
    /// <summary>
    /// Cursos
    /// </summary>
    public partial class Cursos : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 69 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox identifierLabel;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nameLabel;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox quadrLabel;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox yearLabel;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox teacherLabel;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox januaryCheckbox;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox februaryCheckbox;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox marchCheckbox;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox aprilCheckbox;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox mayCheckbox;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox JuneCheckbox;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox julyCheckbox;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox augustCheckbox;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox septemberCheckbox;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox octoberCheckbox;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox novemberCheckbox;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox decemberCheckbox;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox scheduleLabel;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox examDayLabel;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox observationsLabel;
        
        #line default
        #line hidden
        
        
        #line 202 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox paymentsLabel;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox priceMonthLabel;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox hoursLabel;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox priceHoursLabel;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox criteriLabel;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox totalLabel;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\..\Cursos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox UNI_Selector;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CentreEstudisMontilivi;component/cursos.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Cursos.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\Cursos.xaml"
            ((WpfApp1.Cursos)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.DataWindow_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 56 "..\..\..\Cursos.xaml"
            ((System.Windows.Controls.Image)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.goBack);
            
            #line default
            #line hidden
            return;
            case 3:
            this.identifierLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.nameLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.quadrLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.yearLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.teacherLabel = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.januaryCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.februaryCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.marchCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.aprilCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.mayCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 13:
            this.JuneCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 14:
            this.julyCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.augustCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 16:
            this.septemberCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.octoberCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.novemberCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 19:
            this.decemberCheckbox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.scheduleLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.examDayLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.observationsLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.paymentsLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.priceMonthLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.hoursLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.priceHoursLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.criteriLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.totalLabel = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.UNI_Selector = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 30:
            
            #line 236 "..\..\..\Cursos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.updateCourse);
            
            #line default
            #line hidden
            return;
            case 31:
            
            #line 238 "..\..\..\Cursos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.changeNextCourse);
            
            #line default
            #line hidden
            return;
            case 32:
            
            #line 240 "..\..\..\Cursos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.changeBeforeCourse);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

