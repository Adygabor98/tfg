﻿using Comunications;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentreEstudisMontilivi.Visualitzacions.Students
{
    /// <summary>
    /// Lógica de interacción para Students_Screen.xaml
    /// </summary>
    public partial class Students_Screen : UserControl
    {
        public static List<Students> students;
        public static List<Students> actualListStudents; 
        public static ObservableCollection<CourseUser> courses;
        public static ObservableCollection<CourseUser> oldCourses;
        public static BsonDocument courseSearch;
        public static int indexStudent = -1;
        public static string studentSelected = "";
        public static Administrator_Menu objAdmin;
        public Students_Screen(Administrator_Menu obj)
        {
            InitializeComponent();
            students = new List<Students>();
            objAdmin = obj;
            actualListStudents = new List<Students>();
            courses = new ObservableCollection<CourseUser>();
            oldCourses = new ObservableCollection<CourseUser>();
            InitializeView();
        }
        async public void InitializeView()
        {
            List<BsonDocument> studentsInfo = await Requests.GetStudents();

            foreach ( BsonDocument sInfo in studentsInfo)
            {
                Students s = new Students
                {
                    name = sInfo["name"].AsString,
                    surname = sInfo["surname"].AsString,
                    Complete_Name = sInfo["name"] + " " + sInfo["surname"],
                    idStudent = sInfo["_id"].AsObjectId
                };
                students.Add(s);
            }
            actualListStudents = students;
            listStudents.ItemsSource = actualListStudents;

        }
        private void searchStudent(object sender, TextChangedEventArgs e)
        {
            if ( studentSearch.Text != "" ) {
                actualListStudents = new List<Students>();

                foreach (Students s in students)
                {
                    var index = s.surname.ToLower().Contains(studentSearch.Text.ToLower()) ||
                    s.name.ToLower().Contains(studentSearch.Text.ToLower()) || s.Complete_Name.ToLower().Contains(studentSearch.Text.ToLower());

                    if (index)
                    {
                        actualListStudents.Add(s);
                    }
                }
                if ( actualListStudents.Count() == 0)
                {
                    actualListStudents.Add(new Students
                    {
                        Complete_Name = "0 Resultats!"
                    });
                }

            } else
            {
                actualListStudents = students;
            }
            listStudents.ItemsSource = actualListStudents;
        }
        public void addInfoView (object sender = null, RoutedEventArgs e = null)
        {
            courses = new ObservableCollection<CourseUser>();
            oldCourses = new ObservableCollection<CourseUser>();
            ObjectId idStudent;
            if (sender != null) {
                studentSelected = (sender as Label).Content.ToString();
                idStudent = actualListStudents.Find(x => x.Complete_Name == studentSelected).idStudent;
            }
            else
            {
                idStudent = actualListStudents[indexStudent].idStudent;
            }
            addDegrees();
            searchCoursesStudent(idStudent);

            BsonDocument student = Requests.GetStudentById(idStudent);

            initializeViewStudent( student );
        }
        async public void searchCoursesStudent( ObjectId id)
        {
            List<BsonDocument> coursesStudent = await Requests.GetRelationStudents(id);
            foreach(BsonDocument courseS in coursesStudent)
            {
                BsonDocument c = Requests.GetCourseById(courseS["course"].AsObjectId);
                courses.Add(new CourseUser
                {
                    Name = c["name"].AsString,
                    Quadri = c["quadr"].AsString,
                    Code = c["number"].ToInt32(),
                    Year = c["year"].AsString,
                    Id = courseS["course"].AsObjectId
                }) ;
                oldCourses.Add(new CourseUser
                {
                    Name = c["name"].AsString,
                    Quadri = c["quadr"].AsString,
                    Code = c["number"].ToInt32(),
                    Year = c["year"].AsString,
                    Id = courseS["course"].AsObjectId
                }) ;
            }
            lvDataBinding.ItemsSource = courses;
        }
        public async void addDegrees()
        {
            List<BsonDocument> courses = await Requests.GetCarriers();

            foreach (var course in courses) courseLabel.Items.Add(course["name"].ToString());
        }
        public void initializeViewStudent(BsonDocument student)
        {
            numberLabel.Text = student["number"].AsString;
            nameLabel.Text = student["name"].AsString;
            surnameLabel.Text = student["surname"].AsString;
            telLabel.Text = student["phone"].AsString;
            titularLabel.Text = student["titular"].AsString;
            emailLabel.Text = student["email"].AsString;
            bankLabel.Text = student["account"].AsString;
            addressLabel.Text = student["address"].AsString;
            nifLabel.Text = student["nif"].AsString;
            townLabel.Text = student["population"].AsString;
            courseLabel.SelectedItem = student["course"].AsString;
            cpLabel.Text = student["cp"].AsString;
            tel1Label.Text = student["phone1"].AsString;
            tel2Label.Text = student["phone2"].AsString;
            efectiuLabel.IsChecked = student["cash"].AsBoolean;
            telHomeLabel.Text = student["phoneHome"].AsString;
            email1Label.Text = student["email1"].AsString;
            email2Label.Text = student["email2"].AsString;

        }
        private void deleteElementList(object sender, MouseEventArgs e)
        {
            var image = sender as System.Windows.Controls.Image;
            BsonDocument d = image.DataContext.ToBsonDocument();

            int index = courses.Select((elem, index) => new { elem, index }).First(p => p.elem.Code == d["Code"]).index;

            courses.RemoveAt(index);
        }
        private void addCourse(object sender, RoutedEventArgs e)
        {
            courses.Add(new CourseUser()
            {
                Name = (string)courseSearch["name"],
                Code = courseSearch["number"].ToInt32(),
                Year = (string)courseSearch["year"],
                Quadri = (string)courseSearch["quadr"],
                Id = courseSearch["_id"].AsObjectId
            }); ;

            lvDataBinding.ItemsSource = courses;

            codeSearch.Text = "";
            nameCS.Text = "";
        }
        private void searchCourse(object sender, TextChangedEventArgs e)
        {
            BsonDocument result = Requests.GetCourseByNumber(codeSearch.Text);
            if (result != null)
            {
                nameCS.Text = (string)result["name"];
                courseSearch = result;
            }
        }
        private void changeNextStudent(object sender, RoutedEventArgs e)
        {
            courses = new ObservableCollection<CourseUser>();
            if ((indexStudent + 1) < students.Count())
            {
                indexStudent += 1;

                addInfoView();
            }
        }
        private void ChangeBeforeStudent(object sender, RoutedEventArgs e)
        {
            courses = new ObservableCollection<CourseUser>();
            if ((indexStudent - 1) >= 0)
            {
                indexStudent = indexStudent - 1;
                addInfoView();
            }
        }
        private void updateStudent(object sender, RoutedEventArgs e)
        {
            var student = new BsonDocument
            {
                {"number", numberLabel.Text != null ? numberLabel.Text : "" },
                {"name", nameLabel.Text != null ? nameLabel.Text : ""},
                {"surname", surnameLabel.Text != null ? surnameLabel.Text : "" },
                {"phone", telLabel.Text != null ? telLabel.Text : ""},
                {"titular", titularLabel.Text != null ? titularLabel.Text : "" },
                {"email", emailLabel.Text != null ? emailLabel.Text : ""},
                {"account", bankLabel.Text != null ? bankLabel.Text : ""},
                {"address", addressLabel.Text != null ? addressLabel.Text : ""},
                {"nif", nifLabel.Text != null ? nifLabel.Text : ""},
                {"course", courseLabel.SelectedItem != null ? courseLabel.SelectedItem.ToString() : "" },
                {"population", townLabel.Text != null ? townLabel.Text : ""},
                {"cp", cpLabel.Text != null ? cpLabel.Text : ""},
                {"phone1", tel1Label.Text != null ? tel1Label.Text : ""},
                {"phone2", tel2Label.Text != null ? tel2Label.Text : ""},
                {"cash",(bool)efectiuLabel.IsChecked},
                {"phoneHome", telHomeLabel.Text != null ? telHomeLabel.Text : ""},
                {"email1", email1Label.Text != null ? email1Label.Text : ""},
                {"email2", email2Label.Text != null ? email2Label.Text : ""},
            };
            ObjectId idStudent;
            if (indexStudent == -1)
                idStudent = actualListStudents.Find(x => x.Complete_Name == studentSelected).idStudent;
            else
                idStudent = actualListStudents[indexStudent].idStudent;

            Requests.UpdateStudent(idStudent, student);

            // Check if we have to add new courses to the student
            foreach (CourseUser c in courses)
            {
                if (oldCourses.IndexOf(c) == -1)
                {
                    BsonDocument result = Requests.GetCourseById(c.Id);
                    var relation = new BsonDocument
                    {
                        {"course", c.Id },
                        {"student", idStudent },
                        {"teacher", result["teacher"] }
                    };
                    Requests.InsertRelation(relation);
                }
            }

            // Check if we have to delete some course to the student
            foreach (CourseUser c in oldCourses)
            {
                if (courses.IndexOf(c) == -1)
                    Requests.DeleteRelationCourse(c.Id, idStudent);
            }

            BsonDocument options = new BsonDocument
            {
                { "message", "A L U M N E   A C T U A L I T Z A T   C O R R E C T A M E N T!" },
                { "new", "Visualitzar Alumnes" },
                { "matricula", false }
            };
            objAdmin.showSuccessScreenVisualitzation(options, null, this);
        }
        public class Students
        {
            public string name { get; set; }
            public string surname { get; set; }
            public string Complete_Name { get; set; }
            public ObjectId idStudent { get; set; }
        }
        public class CourseUser
        {
            public string Name { get; set; }

            public string Quadri { get; set; }

            public int Code { get; set; }

            public int Hours { get; set; }

            public bool Estat { get; set; }

            public int Price { get; set; }

            public string Year { get; set; }
            public ObjectId Id { get; set; }
        }

    }
}
