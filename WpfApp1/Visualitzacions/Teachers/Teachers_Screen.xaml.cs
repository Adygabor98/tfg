﻿using Comunications;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace CentreEstudisMontilivi.Visualitzacions.Teachers
{
    /// <summary>
    /// Lógica de interacción para Teachers_Screen.xaml
    /// </summary>
    public partial class Teachers_Screen : UserControl
    {
        public static Administrator_Menu objAdmin;
        public static List<BsonDocument> teachers;
        public static List<coursesTeacher> courses;
        public static int teacherIndex = 0;
        public static BsonDocument courseSearch;
        public static List<Teachers> actualListTeachers;

        public Teachers_Screen(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
            teachers = new List<BsonDocument>();
            courses = new List<coursesTeacher>();
            actualListTeachers = new List<Teachers>();
            getTeachers();
        }
        async private void getTeachers()
        {
            teachers = await Requests.GetTeachers();
            foreach (BsonDocument tInfo in teachers)
            {
                Teachers t = new Teachers
                {
                    name = tInfo["name"].AsString,
                    surname = tInfo["surname"].AsString,
                    Complete_Name = tInfo["name"] + " " + tInfo["surname"],
                    idTeacher = tInfo["_id"].AsObjectId
                };
                actualListTeachers.Add(t);
            }
            listTeachers.ItemsSource = actualListTeachers;
            initializeView();
        }
        async public void initializeView()
        {
            if (teachers.Count() > 0)
            {
                nameLabel.Text = teachers[teacherIndex]["name"].AsString;
                surnameLabel.Text = teachers[teacherIndex]["surname"].AsString;
                addressLabel.Text = teachers[teacherIndex]["address"].AsString;
                populationLabel.Text = teachers[teacherIndex]["population"].AsString;
                cpLabel.Text = teachers[teacherIndex]["cp"].AsString;
                telHomeLabel.Text = teachers[teacherIndex]["telHome"].AsString;
                telMobileLabel.Text = teachers[teacherIndex]["mobile"].AsString;
                emailLabel.Text = teachers[teacherIndex]["email"].AsString;
                dniLabel.Text = teachers[teacherIndex]["dni"].AsString;
                titleLabel.Text = teachers[teacherIndex]["title"].AsString;
                yearEntranceLabel.Text = teachers[teacherIndex]["year"].AsString;

                List<BsonDocument> result = await Requests.GetRelationsTeacherCourses(teachers[teacherIndex]["_id"].AsObjectId);

                foreach (BsonDocument course in result)
                {
                    BsonDocument c = Requests.GetCourseById(course["course"].AsObjectId);
                    var index = courses.FindIndex( x => x.Name == c["name"].AsString);
                    if (index == -1)
                    {
                        courses.Add(new coursesTeacher()
                        {
                            Name = c["name"].AsString,
                            Year = c["year"].AsString,
                        });
                    }
                }
                listCourses.ItemsSource = courses;
            }
        }
        public void addInfoView(object sender = null, RoutedEventArgs e = null)
        {
            
        }
        public void saveTeacher(object sender, RoutedEventArgs e)
        {
            var teacher = new BsonDocument
            {
                {"name", nameLabel.Text != null ? nameLabel.Text : "" },
                {"surname", surnameLabel.Text != null ? surnameLabel.Text : "" },
                {"address", addressLabel.Text != null ? addressLabel.Text : "" },
                {"population", populationLabel.Text != null ? populationLabel.Text : "" },
                {"cp", cpLabel.Text != null ? cpLabel.Text : "" },
                {"telHome", telHomeLabel.Text != null ? telHomeLabel.Text : "" },
                {"mobile", telMobileLabel.Text != null ? telMobileLabel.Text : "" },
                {"email", emailLabel.Text != null ? emailLabel.Text : "" },
                {"dni", dniLabel.Text != null ? dniLabel.Text : "" },
                {"title", titleLabel.Text != null ? titleLabel.Text : "" },
                {"year", yearEntranceLabel.Text != null ? yearEntranceLabel.Text : "" },
            };
            Requests.updateTeacher(teachers[teacherIndex]["_id"].AsObjectId, teacher);

            BsonDocument options = new BsonDocument
            {
                { "message", "P R O F E S S O R   A C T U A L I T Z A T   C O R R E C T A M E N T!" },
                { "new", "Visualitzar Professors" },
                { "matricula", false }
            };
            objAdmin.showSuccessScreenVisualitzation(options, this, null);
        }
        private void changeNextTeacher(object sender, RoutedEventArgs e)
        {
            if ((teacherIndex + 1) < teachers.Count())
            {
                teacherIndex += 1;
                courses = new List<coursesTeacher>();
                initializeView();
            }
        }
        private void changeBeforeTeacher(object sender, RoutedEventArgs e)
        {
            if ((teacherIndex - 1) >= 0)
            {
                teacherIndex = teacherIndex - 1;
                courses = new List<coursesTeacher>();
                initializeView();
            }
        }
        private void searchTeacher(object sender, TextChangedEventArgs e)
        {

        }
        public class coursesTeacher
        {
            public string Name { get; set; }

            public string Year { get; set; }
        }
        public class Teachers
        {
            public string name { get; set; }
            public string surname { get; set; }
            public string Complete_Name { get; set; }
            public ObjectId idTeacher { get; set; }
        }
    }
}
