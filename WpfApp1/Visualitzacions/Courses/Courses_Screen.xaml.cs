﻿using CentreEstudisMontilivi.Altes.Courses;
using Comunications;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentreEstudisMontilivi.Visualitzacions.Courses
{
    /// <summary>
    /// Lógica de interacción para Courses_Screen.xaml
    /// </summary>
    public partial class Courses_Screen : UserControl
    {
        public static Administrator_Menu objAdmin; 
        public static List<BsonDocument> courses;
        public static int courseIndex = 0;
        public static Register_Course courseView;
        public Courses_Screen(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
            courses = new List<BsonDocument>();
            getCourses();
        }
        async private void getCourses()
        {
            courses = await Requests.GetCourses(); ;
            initializeView();
        }
        public void initializeView()
        {
            if (courses.Count() > 0)
            {
                courseView = new Register_Course(objAdmin, false, courses[courseIndex]);
                ViewsContainer.Content = courseView;
            }
        }
        private void changeNextCourse(object sender, RoutedEventArgs e)
        {
            if ((courseIndex + 1) < courses.Count())
            {
                courseIndex += 1;
                initializeView();
            } else {
                courseIndex= 0;
                initializeView();
            }
        }
        private void changeBeforeCourse(object sender, RoutedEventArgs e)
        {
            if ((courseIndex - 1) >= 0)
            {
                courseIndex = courseIndex - 1;
                initializeView();
            } else
            {
                courseIndex = courses.Count() -1;
                initializeView();
            }
        }
        private void updateCourse(object sender, RoutedEventArgs e)
        {
            courseView.saveCourse();
        }
    }
}
