﻿using Comunications;
using CsQuery.ExtensionMethods.Internal;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CentreEstudisMontilivi.Visualitzacions.Agenda
{
    /// <summary>
    /// Lógica de interacción para Agenda_Screen.xaml
    /// </summary>
    public partial class Agenda_Screen : UserControl
    {
        public List<BsonDocument> teachers;
        public static string[] daysWeek = { "dl", "dm", "dc", "dj", "dv", "ds", "dg" };
        public static BsonDocument teacher;
        public static BsonDocument agenda;
        public Agenda_Screen()
        {
            InitializeComponent();
            teachers = new List<BsonDocument>();
            initializeView();
        }
        async public void initializeView()
        {
            List<BsonDocument> documents = await Requests.GetTeachers();
            List<string> teachersList = new List<string>();
            foreach (var teacher in documents)
            {
                teachers.Add(teacher);
                var name = teacher["name"].AsString + " " + teacher["surname"].AsString;
                teachersList.Add(name);
            }
            selector.ItemsSource = teachersList;
        }
        List<Class> GetClasses(int actMonth)
        {
            // Getting class information
            List<Class> listClasses = new List<Class>();
            foreach (var course in agenda["dates"].AsBsonArray)
            {
                var dateCourse = course["date"].ToUniversalTime();
                var lastDay = DateTime.DaysInMonth(dateCourse.Year, dateCourse.Month);

                bool condition = dateCourse.Day == lastDay && (dateCourse.Month + 1) == actMonth;
                var day = condition ? 1 : dateCourse.Day;
                var month = dateCourse.Month;
                if (dateCourse.Day == lastDay && (dateCourse.Month + 1) != actMonth) month = month + 1;

                if (month == actMonth || condition)
                {
                    var numberStudents = course["numberStudents"].AsInt32;
                    var courseInfo = Requests.GetCourseById(course["course"].AsObjectId);
                    listClasses.Add(new Class()
                    {
                        day = !condition ? day+1 : day,
                        Students = numberStudents,
                        identifierCourse = courseInfo["name"].AsString,
                        Hours = course["hours"].AsString,
                        price = course["price"].AsDouble
                    });
                }
            }

            return listClasses;
        }
        private void showAgendaTeacher(object sender, RoutedEventArgs e)
        {
            selectTeacher.Visibility = Visibility.Hidden;
            showAgenda.Visibility = Visibility.Visible;

            teacher = teachers[selector.SelectedIndex];
            agenda = Requests.GetAgendaFromTeacher(teacher["_id"].AsObjectId);

            List<Class> listClasses = GetClasses(DateTime.Now.Month);
            generateTable(listClasses);
        }
        async public void generateTable( List<Class> classes)
        {
            // courses
            List<string> coursesIndetifier = await Requests.GetCoursesByTeacher(teacher["_id"].AsObjectId);

            // Create the Grid
            Grid DynamicGrid = new Grid();
            DynamicGrid.Width = 800;
            DynamicGrid.HorizontalAlignment = HorizontalAlignment.Center;
            DynamicGrid.VerticalAlignment = VerticalAlignment.Center;
            DynamicGrid.ShowGridLines = false;
            DynamicGrid.Background = new SolidColorBrush(Colors.Beige);

            // Create Columns
            for (var i = 0; i < coursesIndetifier.Count+1; i++)
            {
                ColumnDefinition gridCol = new ColumnDefinition();
                DynamicGrid.ColumnDefinitions.Add(gridCol);
            }

            // Create Rows
            for (var i = 0; i < 34; i++)
            {
                RowDefinition gridRow = new RowDefinition();
                gridRow.Height = new GridLength(30);
                DynamicGrid.RowDefinitions.Add(gridRow);

                for (var j = 0; j < coursesIndetifier.Count+1; j++)
                {
                    var t = new TextBox();
                    t.BorderThickness = new Thickness(1);
                    t.HorizontalContentAlignment = HorizontalAlignment.Center;
                    t.VerticalContentAlignment = VerticalAlignment.Center;
                    t.Background = new SolidColorBrush(Colors.Beige);

                    // set position inside the grid
                    Grid.SetRow(t, i);
                    Grid.SetColumn(t, j);

                    // Add child to the grid
                    DynamicGrid.Children.Add(t);
                }
            }

            //Add course names to the first row
            for (var i = 1; i <= coursesIndetifier.Count; i++)
            {
                var course = new TextBox();

                // changing styles of the text box
                course.Text = coursesIndetifier[i - 1];
                course.FontSize = 12;
                course.FontWeight = FontWeights.Bold;
                course.BorderThickness = new Thickness(1);
                course.HorizontalContentAlignment = HorizontalAlignment.Center;
                course.VerticalContentAlignment = VerticalAlignment.Center;

                // set position inside the grid
                Grid.SetRow(course, 0);
                Grid.SetColumn(course, i);

                // Add child to the grid
                DynamicGrid.Children.Add(course);
            }

            // Getting month information
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);

            // Get first day of the month
            int indexDay;
            switch (startDate.DayOfWeek.ToString())
            {
                case "Monday": indexDay = 0; break;
                case "Tuesday": indexDay = 1; break;
                case "Wednesday": indexDay = 2; break;
                case "Thursday": indexDay = 3; break;
                case "Friday": indexDay = 4; break;
                case "Saturday": indexDay = 5; break;
                default:
                    indexDay = 6;
                    break;
            }

            // Initialize view grid
            var initialRow = 2; var initialCol = 0;
            List<totalSalary> salary = new List<totalSalary>();
            for (var i = startDate.Day; i <= endDate.Day; i++)
            {
                indexDay = indexDay > 6 ? 0 : indexDay;
                var dayInfo = new TextBox();
                // Change background color if is weekend
                if (indexDay == 5 || indexDay == 6)
                {
                    LinearGradientBrush myBrush = new LinearGradientBrush();
                    myBrush.GradientStops.Add(new GradientStop(Colors.LightGray, 1.0));
                    dayInfo.Background = myBrush;
                    for (var j = 1; j < 10; j++)
                    {
                        var info = new TextBox();
                        info.Background = myBrush;
                        info.BorderThickness = new Thickness(1);
                        Grid.SetRow(info, initialRow);
                        Grid.SetColumn(info, j);
                        DynamicGrid.Children.Add(info);
                    }
                }

                // changing styles of the text box
                dayInfo.Text = daysWeek[indexDay] + " " + i;
                dayInfo.FontSize = 12;
                dayInfo.FontWeight = FontWeights.Bold;
                dayInfo.BorderThickness = new Thickness(1);
                dayInfo.HorizontalContentAlignment = HorizontalAlignment.Center;
                dayInfo.VerticalContentAlignment = VerticalAlignment.Center;

                // set position inside the grid
                Grid.SetRow(dayInfo, initialRow);
                Grid.SetColumn(dayInfo, initialCol);

                // Add child to the grid
                DynamicGrid.Children.Add(dayInfo);

                List<int> indexs = new List<int>(); 
                var indexValue = 0;
                foreach(var c in classes)
                {
                    if (c.day == i) indexs.Add(indexValue);
                    indexValue++;
                }

                if (indexs.Count > 0)
                {
                    foreach(var index in indexs) 
                    { 
                        dayInfo = new TextBox();
                        // changing styles of the text box
                        dayInfo.Text = classes[index].Hours + "h (" + classes[index].Students + ")";
                        dayInfo.FontSize = 15;
                        dayInfo.IsReadOnly = true;
                        dayInfo.Background = new SolidColorBrush(Colors.Beige);
                        dayInfo.Foreground = new SolidColorBrush(Colors.DarkRed);
                        dayInfo.BorderThickness = new Thickness(1);
                        dayInfo.FontWeight = FontWeights.Bold;
                        dayInfo.HorizontalContentAlignment = HorizontalAlignment.Center;
                        dayInfo.VerticalContentAlignment = VerticalAlignment.Center;

                        // set position inside the grid
                        Grid.SetRow(dayInfo, initialRow);
                        var indexCourse = Array.FindIndex<string>(coursesIndetifier.ToArray(), item => item == classes[index].identifierCourse);
                        Grid.SetColumn(dayInfo, indexCourse + 1);

                        // Add child to the grid
                        DynamicGrid.Children.Add(dayInfo);

                        var exist = salary.FindIndex(x => x.row == 33 && x.column == (indexCourse + 1));
                        if (exist == -1)
                        {
                            salary.Add(new totalSalary
                            {
                                row = 33,
                                column = indexCourse + 1,
                                salary = Double.Parse(classes[index].Hours) * classes[index].price,
                                hours = decimal.Parse(classes[index].Hours,
                                    NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands)
                            });
                        }
                        else
                        {
                            salary[exist].salary += Double.Parse(classes[index].Hours) * classes[index].price;
                            salary[exist].hours += decimal.Parse(classes[index].Hours,
                                NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands);
                        }
                    }
                }

                initialRow++;
                indexDay++;
            }

            // Add total salary per course
            foreach (totalSalary t in salary)
            {
                var salaryText = new TextBox();

                // changing styles of the text box
                salaryText.Text = t.salary.ToString() + "€ ( " + t.hours.ToString() + "h )";
                salaryText.FontSize = 12;
                salaryText.FontWeight = FontWeights.Bold;
                salaryText.HorizontalContentAlignment = HorizontalAlignment.Center;
                salaryText.VerticalContentAlignment = VerticalAlignment.Center;

                LinearGradientBrush textColors = new LinearGradientBrush();
                textColors.GradientStops.Add(new GradientStop(Colors.DarkRed, 1.0));
                salaryText.Foreground = textColors;

                // set position inside the grid
                Grid.SetRow(salaryText, t.row);
                Grid.SetColumn(salaryText, t.column);

                // Add child to the grid
                DynamicGrid.Children.Add(salaryText);
            }

            // Add the created grid to the real view
            container.Content = DynamicGrid;
        }

        public class Class
        {
            public int day { get; set; }
            public int Students { get; set; }
            public string Hours { get; set; }
            public string identifierCourse { get; set; }
            public double price { get; set; }
        }
        public class totalSalary
        {
            public int row { get; set; }
            public int column { get; set; }
            public double salary { get; set; }
            public decimal hours { get; set; }
        }
    }
}
