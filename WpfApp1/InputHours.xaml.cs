﻿using Comunications;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para InputHours.xaml
    /// </summary>
    public partial class InputHours : Window
    {
        public static int day, month, year;
        public const string code0 = "Tots els Alumnes Paguen";
        public const string code1 = "Algún Alumne no Paga";

        public static ObjectId teacher;
        public static List<ObjectId> incStudents = new List<ObjectId>();
        public static List<ObjectId> studentsClass = new List<ObjectId>();

        public InputHours(ObjectId t, int d, int m, int y)
        {
            InitializeComponent();
            day = d; month = m; year = y;
            teacher = t;
            initializeViewDone();

            text2.Items.Add(code0);
            text2.Items.Add(code1);

            text2.Text = code0;

            courseAdd.Content = "Escull un Grup";

            DateTime dateValue = new DateTime(year, month+1, day);
            title.Content = dateValue.ToString("MMMM dd, yyyy");
        }

        async private void InitializeView ()
        {
            DateTime date = new DateTime(year, month + 1, day);

            List<BsonDocument> result = await Requests.GetRelationsTeacherCourses(teacher);
            if (result != null)
            {
                List<Group> llistaGrups = new List<Group>();
                foreach (BsonDocument course in result)
                {
                    BsonDocument c = Requests.GetCourseById( course["course"].AsObjectId );
                    List<BsonDocument> s = await Requests.GetStudentsById( course["course"].AsObjectId );

                    string allStudents = "";
                    foreach (BsonDocument student in s)
                    {
                        BsonDocument alumne = Requests.GetStudentById( student["student"].AsObjectId );
                        allStudents += alumne["name"] + ", ";
                    }
                    allStudents = allStudents.Substring(0, allStudents.Length - 1);

                    bool alreadyExists = llistaGrups.Any(x => x.Code == c["number"]);
                    if (!alreadyExists)
                    {
                        allStudents = allStudents.Remove(allStudents.Length - 1);

                        llistaGrups.Add(new Group()
                        {
                            Name = c["name"].AsString,
                            Code = c["number"].AsString,
                            Students = allStudents
                        });
                    }

                }
                listGroups.ItemsSource = llistaGrups;
            }

        }

        private void getCourses(object sender, RoutedEventArgs e)
        {
            if (showCourses.Content.ToString() == "Visualitza Classes")
            {
                input.Visibility = Visibility.Hidden;
                view.Visibility = Visibility.Visible;
                showCourses.Content = "Afegeix Classes";
                initializeViewDone();
            } else
            {
                input.Visibility = Visibility.Visible;
                view.Visibility = Visibility.Hidden;
                showCourses.Content = "Visualitza Classes";
                InitializeView();
            } 
        }

        public void initializeViewDone()
        {
            List<GroupDone> listGroup = new List<GroupDone>();

            BsonDocument courses = Requests.GetAgendaFromTeacher(teacher);
            foreach (BsonDocument course in courses["dates"].AsBsonArray)
            {
                var groupDate = course["date"].ToUniversalTime();
                var dayCourse = groupDate.Day + 1;
                var monthCourse = groupDate.Month;
                if (DateTime.DaysInMonth(groupDate.Year, groupDate.Month) == 30 && groupDate.Day == 30 )
                {
                    dayCourse = 1; 
                    monthCourse = groupDate.Month + 1;
                } else if (DateTime.DaysInMonth(groupDate.Year, groupDate.Month) == 31 && groupDate.Day == 31 )
                {
                    dayCourse = 1;
                    monthCourse = groupDate.Month + 1;
                }
                    

                if (day == (dayCourse) && (month + 1) == monthCourse && year == groupDate.Year)
                {
                    BsonDocument c = Requests.GetCourseById(course["course"].AsObjectId);
                    string s = "";

                    foreach (ObjectId student in course["students"].AsBsonArray)
                    {
                        BsonDocument studentObj = Requests.GetStudentById(student);
                        s += studentObj["name"].AsString + ", ";
                    }
                    s = s.Substring(0, s.Length - 1);

                    listGroup.Add(new GroupDone()
                    {
                        Name = c["name"].AsString,
                        Code = c["number"].AsString,
                        Students = s,
                        Duration = decimal.Parse(course["hours"].AsString,
                            NumberStyles.AllowDecimalPoint | NumberStyles.Number | NumberStyles.AllowThousands)
                    });
                }
            }
            if (listGroup.Count() == 0) { infoLabel.Visibility = Visibility.Visible; listClassDone.Visibility = Visibility.Hidden; }
            else { infoLabel.Visibility = Visibility.Hidden; listClassDone.Visibility = Visibility.Visible; }

            listClassDone.ItemsSource = listGroup;
        }

        async private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmb = sender as ComboBox;
            if ( cmb.SelectedItem.ToString() == code1 )
            {
                BsonDocument course = Requests.GetCourseByName( courseAdd.Content.ToString() );
                List<BsonDocument> objects = await Requests.GetStudentsById(course["_id"].AsObjectId);
                foreach (BsonDocument student in objects)
                {
                    BsonDocument resultS = Requests.GetStudentById( student["student"].AsObjectId );
                    studentsClass.Add(resultS["_id"].AsObjectId);

                    text3.Items.Add( new Student { Name = resultS["name"].AsString, Surname = resultS["surname"].AsString,
                                                    id = resultS["_id"].AsObjectId} );
                }
                
                text3.Visibility = Visibility.Visible; label3.Visibility = Visibility.Visible;
            } else
            {
                text3.Visibility = Visibility.Hidden; label3.Visibility = Visibility.Hidden;
            }
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as System.Windows.Controls.CheckBox;

            var courseName = ((WpfApp1.InputHours.Group)checkbox.DataContext).Name;

            label4.Visibility = Visibility.Visible; text4.Visibility = Visibility.Visible;
            courseAdd.Content= courseName ; text2.Visibility = Visibility.Visible;
            button.Visibility = Visibility.Visible; label1.Visibility = Visibility.Visible;
            label2.Visibility = Visibility.Visible; text1.Visibility = Visibility.Visible;
            hoursText.Visibility = Visibility.Visible; studentsText.Visibility = Visibility.Visible;
        }

        private void CheckBoxUnChanged(object sender, RoutedEventArgs e)
        {
            courseAdd.Content = "Escull un Grup"; text2.Visibility = Visibility.Hidden;
            label4.Visibility = Visibility.Hidden; text4.Visibility = Visibility.Hidden;
            button.Visibility = Visibility.Hidden; label1.Visibility = Visibility.Hidden;
            label2.Visibility = Visibility.Hidden; text1.Visibility = Visibility.Hidden;
            text3.SelectedIndex = -1; text2.Text = code0;
            text3.Items.Clear(); text3.Visibility = Visibility.Hidden; label3.Visibility = Visibility.Hidden;
            hoursText.Visibility = Visibility.Hidden; studentsText.Visibility = Visibility.Hidden;
        }

        private void addIncStudent(object sender, RoutedEventArgs e)
        {
            var checkBox = e.OriginalSource as CheckBox;
            var index = Array.FindIndex<Student>(text3.Items.Cast<Student>().ToArray<Student>(), item => item.Name == ((Student)checkBox.DataContext).Name && item.Surname == ((Student)checkBox.DataContext).Surname);

            incStudents.Add(studentsClass[index]);
        }

        private void deleteIncStudent(object sender, RoutedEventArgs e)
        {
            var checkBox = e.OriginalSource as CheckBox;
            var index = Array.FindIndex<Student>(text3.Items.Cast<Student>().ToArray<Student>(), item => item.Name == ((Student)checkBox.DataContext).Name && item.Surname == ((Student)checkBox.DataContext).Surname);

            incStudents.Remove(studentsClass[index]);
        }

        async public void updateAgenda (BsonDocument agenda)
        {
            BsonDocument course = Requests.GetCourseByName( courseAdd.Content.ToString() );
            List<BsonDocument> objects = await Requests.GetStudentsById(course["_id"].AsObjectId);

            DateTime date = new DateTime(year, month + 1, day);
            BsonArray students = new BsonArray();
            foreach (BsonDocument student in objects)
            {
                students.Add(student["student"]);
            }

            int incidence = 0;
            BsonArray incidenceStudents = new BsonArray();
            if (text2.SelectedItem.ToString() == code1)
            {
                incidence = 1;
                foreach (ObjectId s in incStudents) incidenceStudents.Add(s);
            }
            string hours = text1.Text.Split(" ")[0];
            int numStudents = incidence==1 ? Int32.Parse(text4.Text.Split(" ")[0]) - incStudents.Count() : Int32.Parse(text4.Text.Split(" ")[0]);

            Double price = Requests.GetPriceForTeacher( numStudents, course["type"].AsBoolean );

            BsonArray updatedArray = agenda["dates"].AsBsonArray
                    .Add(new BsonDocument { { "date", date }, { "course", course["_id"] }, { "students", students }, { "hours", hours },
                        {"numberStudents", numStudents }, { "codeIncidence", incidence }, { "incStudents", incidenceStudents },
                        {"price", price } });

            var arrayFilter = Builders<BsonDocument>.Filter.Eq("_id", agenda["_id"]);
            var arrayUpdate = Builders<BsonDocument>.Update.Set("dates", updatedArray);

            Requests.UpdateAgenda( arrayFilter, arrayUpdate );

            text1.Text = ""; text4.Text = "";
            CheckBoxUnChanged(new Object(), new RoutedEventArgs());
        }

        private void SubscribeCourse(object sender, RoutedEventArgs e)
        {
            BsonDocument agenda = Requests.GetAgendaFromTeacher(teacher);
            updateAgenda(agenda);
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
        }
        public class Group
        {
            public string Name { get; set; }

            public string Code { get; set; }

            public string Students { get; set; }
        }

        public class GroupDone
        {
            public string Name { get; set; }

            public string Code { get; set; }

            public string Students { get; set; }
            public decimal Duration { get; set; }
        }

        public class Student
        {
            public string Name { get; set; }
            public string Surname { get; set; }
            public ObjectId id { get; set; }
        }
    }
}
