﻿using Comunications;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace CentreEstudisMontilivi.Altes.Courses
{
    /// <summary>
    /// Lógica de interacción para Register_Course.xaml
    /// </summary>
    public partial class Register_Course : UserControl
    {
        public static Administrator_Menu objAdmin;
        public static List<BsonDocument> teachersAvailable;
        public static bool showButtons;
        public static BsonDocument course;
        public Register_Course(Administrator_Menu obj, bool buttons = true, BsonDocument courseParameter = null)
        {
            InitializeComponent();
            objAdmin = obj;
            showButtons = buttons;
            course = courseParameter;
            if (course != null)
                initialitzeViewWithCourse();
            else
                initializeView();
        }
        public void initializeView(object sender = null, RoutedEventArgs e = null)
        {
            teachersAvailable = new List<BsonDocument>();
            getTeachersAndIdentifiers();

            identifierLabel.Text = "";
            nameLabel.Text = "";
            quadrLabel.Text = "";
            yearLabel.Text = "";
            teacherLabel.SelectedIndex = - 1;
            scheduleLabel.Text = "";
            examDayLabel.Text = "";
            observationsLabel.Text = "";
            paymentsLabel.Text = "";
            priceMonthLabel.Text = "";
            Hours_Selector.IsChecked = false;
            UNI_Selector.IsChecked = false;
            januaryCheckbox.IsChecked = false;
            februaryCheckbox.IsChecked = false;
            marchCheckbox.IsChecked = false;
            aprilCheckbox.IsChecked = false;
            mayCheckbox.IsChecked = false;
            JuneCheckbox.IsChecked = false;
            julyCheckbox.IsChecked = false;
            augustCheckbox.IsChecked = false;
            septemberCheckbox.IsChecked = false;
            octoberCheckbox.IsChecked = false;
            novemberCheckbox.IsChecked = false;
            decemberCheckbox.IsChecked = false;
            buttonsRow.Visibility = showButtons ? Visibility.Visible : Visibility.Hidden;
        }
        public void initialitzeViewWithCourse ()
        {
            teachersAvailable = new List<BsonDocument>();
            getTeachersAndIdentifiers();
            BsonDocument teacher = Requests.GetTeacherById(course["teacher"].AsObjectId);

            identifierLabel.Text = course["number"].AsString;
            nameLabel.Text = course["name"].AsString;
            quadrLabel.Text = course["quadr"].AsString;
            yearLabel.Text = course["year"].AsString;
            teacherLabel.SelectedItem = teacher["name"].AsString + " " + teacher["surname"].AsString;
            scheduleLabel.Text = course["schedule"].AsString; ;
            examDayLabel.Text = course["examDay"].AsString; ;
            observationsLabel.Text = course["observations"].AsString; ;
            paymentsLabel.Text = course["payments"].AsString; ;
            priceMonthLabel.Text = course["priceMonth"].AsString; ;
            Hours_Selector.IsChecked = course["typeCourse"].AsBoolean;
            UNI_Selector.IsChecked = course["type"].AsBoolean;
            januaryCheckbox.IsChecked = course["months"][0]["january"].AsBoolean;
            februaryCheckbox.IsChecked = course["months"][1]["february"].AsBoolean;
            marchCheckbox.IsChecked = course["months"][2]["march"].AsBoolean;
            aprilCheckbox.IsChecked = course["months"][3]["april"].AsBoolean;
            mayCheckbox.IsChecked = course["months"][4]["may"].AsBoolean;
            JuneCheckbox.IsChecked = course["months"][5]["june"].AsBoolean;
            julyCheckbox.IsChecked = course["months"][6]["july"].AsBoolean;
            augustCheckbox.IsChecked = course["months"][7]["august"].AsBoolean;
            septemberCheckbox.IsChecked = course["months"][8]["september"].AsBoolean;
            octoberCheckbox.IsChecked = course["months"][9]["october"].AsBoolean;
            novemberCheckbox.IsChecked = course["months"][10]["november"].AsBoolean;
            decemberCheckbox.IsChecked = course["months"][11]["december"].AsBoolean;
            buttonsRow.Visibility = Visibility.Hidden;
        }
        async private void getTeachersAndIdentifiers()
        {
            List<BsonDocument> documents = await Requests.GetTeachers();
            foreach (var teacher in documents)
            {
                var name = teacher["name"].AsString + " " + teacher["surname"].AsString;
                teachersAvailable.Add(teacher);
                teacherLabel.Items.Add(name);
            }
            if (course == null)
            {
                List<BsonDocument> documentsCourses = await Requests.GetCourses();
                identifierLabel.Text = (documentsCourses.Count + 1).ToString();
            }
        }
        public void saveCourse(object sender = null, RoutedEventArgs e = null)
        {
            BsonDocument teacher = teachersAvailable[teacherLabel.SelectedIndex];
            var courseDocument = new BsonDocument
            {
                {"number", identifierLabel.Text },
                {"name", nameLabel.Text != null ? nameLabel.Text : ""},
                {"quadr", quadrLabel.Text != null ? quadrLabel.Text : "" },
                {"year", yearLabel.Text != null ? yearLabel.Text : ""},
                {"teacher", teacher["_id"] },
                {"schedule", scheduleLabel.Text != null ? scheduleLabel.Text : ""},
                {"examDay", examDayLabel.Text != null ? examDayLabel.Text : ""},
                {"observations", observationsLabel.Text != null ? observationsLabel.Text : ""},
                {"payments", paymentsLabel.Text != null ? paymentsLabel.Text : ""},
                {"priceMonth", priceMonthLabel.Text != null ? priceMonthLabel.Text : ""},
                {"typeCourse", Hours_Selector.IsChecked},
                {"type", UNI_Selector.IsChecked}, // true = UNI, false = OTHER
                {"months", new BsonArray {
                    new BsonDocument { { "january", januaryCheckbox.IsChecked } },
                    new BsonDocument { { "february", februaryCheckbox.IsChecked } },
                    new BsonDocument { { "march", marchCheckbox.IsChecked } },
                    new BsonDocument { { "april", aprilCheckbox.IsChecked } },
                    new BsonDocument { { "may", mayCheckbox.IsChecked } },
                    new BsonDocument { { "june", JuneCheckbox.IsChecked } },
                    new BsonDocument { { "july", julyCheckbox.IsChecked } },
                    new BsonDocument { { "august", augustCheckbox.IsChecked } },
                    new BsonDocument { { "september", septemberCheckbox.IsChecked } },
                    new BsonDocument { { "october", octoberCheckbox.IsChecked } },
                    new BsonDocument { { "november", novemberCheckbox.IsChecked } },
                    new BsonDocument { { "december", decemberCheckbox.IsChecked } },
                } }
            };

            if (course == null )
            { // afegim un curs
                Requests.InsertCourse(courseDocument);

                BsonDocument options = new BsonDocument
                {
                    { "message", "C U R S   D O N A T   D ' A L T A  C O R R E C T A M E N T!" },
                    { "new", "Nova Alta Curs" },
                    { "matricula", false }
                };
                objAdmin.showSuccessScreen(options, null, null, this, null);
            } else // Visualitzem un curs
            {
                Requests.UpdateCourse(course["_id"].AsObjectId, courseDocument);
                BsonDocument options = new BsonDocument
                {
                    { "message", "C U R S   A C T U A L I T Z A T    C O R R E C T A M E N T!" },
                    { "new", "Visualitzar Cursos" },
                    { "matricula", false }
                };
                objAdmin.showSuccessScreen(options, null, null, this, null);
            }

        }
    }
}
