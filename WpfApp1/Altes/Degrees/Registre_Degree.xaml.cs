﻿using Comunications;
using MongoDB.Bson;
using System.Windows;
using System.Windows.Controls;
namespace CentreEstudisMontilivi.Altes.Degrees
{
    /// <summary>
    /// Lógica de interacción para Registre_Degree.xaml
    /// </summary>
    public partial class Registre_Degree : UserControl
    {
        public static Administrator_Menu objAdmin;
        public Registre_Degree(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
        }
        private void saveDegree(object sender, RoutedEventArgs e)
        {
            BsonDocument carrier = new BsonDocument
                {
                    {"name", nameLabel.Text },
                };
            Requests.InsertCarrier(carrier);

            BsonDocument options = new BsonDocument
            {
                { "message", "C A R R E R A   D O N A D A   D ' A L T A  C O R R E C T A M E N T!" },
                { "new", "Nova Alta Carrera" },
                { "matricula", false }
            };
            objAdmin.showSuccessScreen(options, null, null, null, this);
        }
    }
}
