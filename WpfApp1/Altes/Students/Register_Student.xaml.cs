﻿using ceTe.DynamicPDF.PageElements.Charting;
using Comunications;
using HtmlAgilityPack;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CentreEstudisMontilivi.Altes.Students
{
    /// <summary>
    /// Lógica de interacción para Register_Student.xaml
    /// </summary>
    public partial class Register_Student : UserControl
    {
        public static BsonDocument courseSearched;
        public static ObservableCollection<CourseUser> items;
        public static List<ObjectId> coursesStudent;
        public static Administrator_Menu objAdmin;

        public Register_Student(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
            addCourses();
            initialitzeView();
        }
        public void initialitzeView(object sender = null, RoutedEventArgs e = null)
        {
            items = new ObservableCollection<CourseUser>();
            coursesStudent = new List<ObjectId>();
            clearForm();
            lvDataBinding.ItemsSource = items;
            getStudents();
        }
        public void clearForm ()
        {
            numberLabel.Text = "";
            nameLabel.Text = "";
            surnameLabel.Text = "";
            telLabel.Text ="";
            titularLabel.Text = "";
            emailLabel.Text = "";
            bankLabel.Text = "";
            addressLabel.Text = "";
            nifLabel.Text = "";
            townLabel.Text = "";
            courseLabel.SelectedIndex = -1;
            cpLabel.Text = "";
            tel1Label.Text = "";
            tel2Label.Text = "";
            efectiuLabel.IsChecked = false;
            telHomeLabel.Text = "";
            email1Label.Text = "";
            email2Label.Text = "";
        }
        public async void addCourses()
        {
            List<BsonDocument> courses = await Requests.GetCarriers();

            foreach (var course in courses) courseLabel.Items.Add(course["name"].ToString());
        }
        async private void getStudents()
        {
            List<BsonDocument> documentsStudents = await Requests.GetStudents();
            numberLabel.Text = (documentsStudents.Count + 1).ToString();
        }
        private void saveStudent(object sender, RoutedEventArgs e)
        {
            var student = new BsonDocument
            {
                {"number", numberLabel.Text != null ? numberLabel.Text : "" },
                {"name", nameLabel.Text != null ? nameLabel.Text : ""},
                {"surname", surnameLabel.Text != null ? surnameLabel.Text : "" },
                {"phone", telLabel.Text != null ? telLabel.Text : ""},
                {"titular", titularLabel.Text != null ? titularLabel.Text : "" },
                {"email", emailLabel.Text != null ? emailLabel.Text : ""},
                {"account", bankLabel.Text != null ? bankLabel.Text : ""},
                {"address", addressLabel.Text != null ? addressLabel.Text : ""},
                {"nif", nifLabel.Text != null ? nifLabel.Text : ""},
                {"population", townLabel.Text != null ? townLabel.Text : ""},
                {"course", (string)courseLabel.SelectedItem != null ? (string)courseLabel.SelectedItem : ""},
                {"cp", cpLabel.Text != null ? cpLabel.Text : ""},
                {"phone1", tel1Label.Text != null ? tel1Label.Text : ""},
                {"phone2", tel2Label.Text != null ? tel2Label.Text : ""},
                {"cash",(bool)efectiuLabel.IsChecked},
                {"phoneHome", telHomeLabel.Text != null ? telHomeLabel.Text : ""},
                {"email1", email1Label.Text != null ? email1Label.Text : ""},
                {"email2", email2Label.Text != null ? email2Label.Text : ""},
            };
            Requests.InsertStudent(student);

            foreach (ObjectId c in coursesStudent)
            {
                BsonDocument result = Requests.GetCourseById(c);
                var relation = new BsonDocument
                {
                    {"course", c },
                    {"student", student["_id"].AsBsonValue },
                    {"teacher", result["teacher"] }
                };
                Requests.InsertRelation(relation);
            }
            BsonDocument options = new BsonDocument
            {
                { "message", "A L U M N E   D O N A T   D ' A L T A  C O R R E C T A M E N T!" },
                { "new", "Nova Alta Alumne" },
                { "matricula", true }
            };
            objAdmin.showSuccessScreen(options, this, null, null, null);

        }
        private void addCourse(object sender, RoutedEventArgs e)
        {

            coursesStudent.Add(courseSearched["_id"].AsObjectId);
            items.Add(new CourseUser()
            {
                Name = (string)courseSearched["name"],
                Code = courseSearched["number"].ToInt32(),
                Year = (string)courseSearched["year"],
                Quadri = (string)courseSearched["quadr"]
            }); ;

            lvDataBinding.ItemsSource = items;

            codeSearch.Text = "";
            nameCS.Text = "";
        }
        private void searchCourse(object sender, TextChangedEventArgs e)
        {
            BsonDocument result = Requests.GetCourseByNumber(codeSearch.Text);
            if (result != null)
            {
                nameCS.Text = (string)result["name"];
                courseSearched = result;
            }
        }
        private void deleteElementList(object sender, MouseEventArgs e)
        {
            var image = sender as System.Windows.Controls.Image;
            BsonDocument d = image.DataContext.ToBsonDocument();

            int index = items.Select((elem, index) => new { elem, index }).First(p => p.elem.Code == d["Code"]).index;

            items.RemoveAt(index);
        }
        public void imprimirMatricula(object sender = null, EventArgs e = null)
        {
            // This will get the current WORKING directory (i.e. \bin\Debug)
            string workingDirectory = Environment.CurrentDirectory;

            // This will get the current PROJECT directory
            string directory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            HtmlDocument doc = new HtmlDocument();
            doc.Load(directory+"\\WpfApp1\\Templates\\Fitxa de Matriculacio.html");

            doc.GetElementbyId("name").InnerHtml = nameLabel.Text;
            doc.GetElementbyId("surname").InnerHtml = surnameLabel.Text;
            doc.GetElementbyId("email").InnerHtml = emailLabel.Text;
            doc.GetElementbyId("email1").InnerHtml = email1Label.Text;
            doc.GetElementbyId("email2").InnerHtml = email2Label.Text;
            doc.GetElementbyId("tel1").InnerHtml = tel1Label.Text;
            doc.GetElementbyId("telHome").InnerHtml = telHomeLabel.Text;
            doc.GetElementbyId("tel2").InnerHtml = tel2Label.Text;
            doc.GetElementbyId("phone").InnerHtml = telLabel.Text;
            doc.GetElementbyId("nif").InnerHtml = nifLabel.Text;
            doc.GetElementbyId("address").InnerHtml = addressLabel.Text;
            doc.GetElementbyId("population").InnerHtml = townLabel.Text;
            doc.GetElementbyId("carrera").InnerHtml = (string)courseLabel.SelectedItem != null ? (string)courseLabel.SelectedItem : "";
            doc.GetElementbyId("cp").InnerHtml = cpLabel.Text;

            var a = doc.GetElementbyId("table");
            HtmlNode header = HtmlNode.CreateNode("<tr style='font-weight: bold;color: #7F3234'><td>" + "Nom del Curs" + "</td><th>" + "Hores" + "</th><th>" + "Any" + "</th><th>" + "Quatrimestre" + "</th></tr>");
            a.AppendChild(header);

            foreach (CourseUser c in items)
            {
                HtmlNode course = HtmlNode.CreateNode("<tr><td>" + c.Name + "</td><td style='text-align: center;'>" + c.Hours + "</td><td style='text-align: center;'>" + c.Year + "</td><td style='text-align: center;'>" + c.Quadri + "</td></tr>");
                a.AppendChild(course);
            }

            doc.GetElementbyId("titular").InnerHtml = titularLabel.Text;
            doc.GetElementbyId("bankCode").InnerHtml = bankLabel.Text;

            var divCheck = doc.GetElementbyId("efectiu");
            var HTML = "";
            if ((bool)efectiuLabel.IsChecked) HTML += "<input type='checkbox' checked>";
            else HTML += "<input type='checkbox'>";
            divCheck.InnerHtml = HTML;


            doc.Save(directory+"\\WpfApp1\\Matricules\\" + nameLabel.Text + "-matricula.html");

            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = directory+"\\WpfApp1\\Matricules\\" + nameLabel.Text + "-matricula.html";
            info.UseShellExecute = true;
            Process.Start(info);
        }
        public class CourseUser
        {
            public string Name { get; set; }

            public string Quadri { get; set; }

            public int Code { get; set; }

            public int Hours { get; set; }

            public bool AltaBaixa { get; set; }

            public int Price { get; set; }

            public string Year { get; set; }
        }

        private void showWelcomeScreen(object sender, RoutedEventArgs e)
        {
            objAdmin.OpenDefaultView("Escull una opció per començar");
        }
    }
}
