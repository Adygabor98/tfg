﻿using Comunications;
using MongoDB.Bson;
using System.Windows;
using System.Windows.Controls;

namespace CentreEstudisMontilivi.Altes.Teachers
{
    /// <summary>
    /// Lógica de interacción para Register_Teacher.xaml
    /// </summary>
    public partial class Register_Teacher : UserControl
    {
        public static Administrator_Menu objAdmin;
        public Register_Teacher(Administrator_Menu obj)
        {
            InitializeComponent();
            objAdmin = obj;
            initialitzeView();
        }
        public void initialitzeView(object sender = null, RoutedEventArgs e = null)
        {
            nameLabel.Text = "";
            surnameLabel.Text = "";
            addressLabel.Text = "";
            populationLabel.Text = "";
            cpLabel.Text = "";
            telHomeLabel.Text = "";
            telMobileLabel.Text = "";
            emailLabel.Text = "";
            dniLabel.Text = "";
            titleLabel.Text = "";
            yearEntranceLabel.Text = "";
        }
        private void saveTeacher(object sender, RoutedEventArgs e)
        {
            var teacher = new BsonDocument
            {
                {"name", nameLabel.Text != null ? nameLabel.Text : "" },
                {"surname", surnameLabel.Text != null ? surnameLabel.Text : "" },
                {"address", addressLabel.Text != null ? addressLabel.Text : "" },
                {"population", populationLabel.Text != null ? populationLabel.Text : "" },
                {"cp", cpLabel.Text != null ? cpLabel.Text : "" },
                {"telHome", telHomeLabel.Text != null ? telHomeLabel.Text : "" },
                {"mobile", telMobileLabel.Text != null ? telMobileLabel.Text : "" },
                {"email", emailLabel.Text != null ? emailLabel.Text : "" },
                {"dni", dniLabel.Text != null ? dniLabel.Text : "" },
                {"title", titleLabel.Text != null ? titleLabel.Text : "" },
                {"year", yearEntranceLabel.Text != null ? yearEntranceLabel.Text : "" },
            };
            Requests.InsertTeacher(teacher);
            BsonArray course = new BsonArray { };
            var agenda = new BsonDocument
            {
                {"ProfessorId", teacher["_id"] },
            };
            agenda.Add("dates", course);

            Requests.InsertAgenda(agenda);

            BsonDocument options = new BsonDocument
            {
                { "message", "P R O F E S S O R   D O N A T   D ' A L T A  C O R R E C T A M E N T!" },
                { "new", "Nova Alta Professor" },
                { "matricula", false }
            };
            objAdmin.showSuccessScreen(options, null, this, null, null);
        }
    }
}
