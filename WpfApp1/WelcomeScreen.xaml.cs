﻿using System.Windows.Controls;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para WelcomeScreen.xaml
    /// </summary>
    public partial class WelcomeScreen : UserControl
    {
        public WelcomeScreen(string mess)
        {
            InitializeComponent();
            message.Text = mess;
        }
    }
}
