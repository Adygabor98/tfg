﻿using Comunications;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace CentreEstudisMontilivi
{
    /// <summary>
    /// Lógica de interacción para inputHours_Screen.xaml
    /// </summary>
    public partial class inputHours_Screen : UserControl
    {
        public static Administrator_Menu objAdmin;
        public static BsonDocument teacher;
        public static List<Group> groups;
        public static BsonDocument agenda;
        public static Group group;
        public static int Day;
        public inputHours_Screen(int day, Administrator_Menu obj, BsonDocument t)
        {
            InitializeComponent();
            objAdmin = obj;
            teacher = t;
            Day = day;
            initializeView();
        }
        async public void initializeView()
        {
            agenda = Requests.GetAgendaFromTeacher(teacher["_id"].AsObjectId);

            List<BsonDocument> result = await Requests.GetRelationsTeacherCourses(teacher["_id"].AsObjectId);
            if (result != null)
            {
                List<Group> llistaGrups = new List<Group>();
                foreach (BsonDocument course in result)
                {
                    BsonDocument c = Requests.GetCourseById(course["course"].AsObjectId);
                    List<BsonDocument> s = await Requests.GetStudentsById(course["course"].AsObjectId);

                    string allStudents = "";
                    int numStudents = 0;
                    List<ObjectId> idStudents = new List<ObjectId>();
                    foreach (BsonDocument student in s)
                    {
                        BsonDocument alumne = Requests.GetStudentById(student["student"].AsObjectId);
                        allStudents += alumne["name"] + ", ";
                        idStudents.Add(alumne["_id"].AsObjectId);
                        numStudents++;

                    }
                    allStudents = allStudents.Substring(0, allStudents.Length - 1);

                    bool alreadyExists = llistaGrups.Any(x => x.Code == c["number"]);
                    if (!alreadyExists)
                    {
                        allStudents = allStudents.Remove(allStudents.Length - 1);

                        llistaGrups.Add(new Group()
                        {
                            Name = c["name"].AsString,
                            Code = c["number"].AsString,
                            Students = allStudents,
                            Id = c["_id"].AsObjectId,
                            StudentsId = idStudents,
                            NumStudents = numStudents,
                        });
                    }

                }
                groups = llistaGrups;
                coursesList.ItemsSource = llistaGrups;
            }
        }
        public void showFormToInput(object sender, RoutedEventArgs e)
        {
            var code = ((Button)sender).Tag;

            group = groups[groups.FindIndex(x => x.Code == code.ToString())];

            titleCourse.Content = group.Name;
            formInputView.Visibility = Visibility.Visible;
        }
        async public void inputarHores(object sender, RoutedEventArgs e)
        {
            BsonDocument course = Requests.GetCourseByName(group.Name);
            List<BsonDocument> objects = await Requests.GetStudentsById( group.Id );
            var now = DateTime.Now;
            DateTime date = new DateTime(now.Year, now.Month, Day);
            BsonArray students = new BsonArray();
            foreach (BsonDocument student in objects)
            {
                students.Add(student["student"]);
            }

            /*int incidence = 0;
            BsonArray incidenceStudents = new BsonArray();
            if (text2.SelectedItem.ToString() == code1)
            {
                incidence = 1;
                foreach (ObjectId s in incStudents) incidenceStudents.Add(s);
            }*/
            string hours = durationClass.Text;
            // int numStudents = incidence == 1 ? Int32.Parse(text4.Text.Split(" ")[0]) - incStudents.Count() : Int32.Parse(text4.Text.Split(" ")[0]);
            int numStudents = Int32.Parse(numberStudents.Text);

            Double price = Requests.GetPriceForTeacher(numStudents, course["type"].AsBoolean);

            BsonArray updatedArray = agenda["dates"].AsBsonArray
                    .Add(new BsonDocument { { "date", date }, { "course", course["_id"] }, { "students", students }, { "hours", hours },
                        {"numberStudents", numStudents }, { "codeIncidence", 0 }, { "incStudents", "" },
                        {"price", price } });

            var arrayFilter = Builders<BsonDocument>.Filter.Eq("_id", agenda["_id"]);
            var arrayUpdate = Builders<BsonDocument>.Update.Set("dates", updatedArray);

            Requests.UpdateAgenda(arrayFilter, arrayUpdate);

            durationClass.Text = ""; numberStudents.Text = "";

            objAdmin.openRefreshCalendar(teacher);
        }
        public class Group
        {
            public string Name { get; set; }
            public string Code { get; set; }
            public string Students { get; set; }
            public int NumStudents { get; set; }
            public ObjectId Id { get; set; }
            public List<ObjectId> StudentsId {get; set;}
        }
    }
}
